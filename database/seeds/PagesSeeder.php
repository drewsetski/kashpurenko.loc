<?php

use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'name' => 'Главная',
            'slug' => 'index',
            'type' => 'main',
            'body' => '',
            'additional_body' => '',
            'meta_title' => 'Кашпуренко Владислав',
            'meta_keywords' => '',
            'meta_description' => '',
        ]);
        DB::table('pages')->insert([
            'name' => 'Биография',
            'slug' => 'biography',
            'type' => 'main',
            'body' => '',
            'additional_body' => '',
            'meta_title' => 'Биография',
            'meta_keywords' => '',
            'meta_description' => '',
        ]);
        DB::table('pages')->insert([
            'name' => 'Новости',
            'slug' => 'news',
            'type' => 'main',
            'body' => '',
            'additional_body' => '',
            'meta_title' => 'Новости',
            'meta_keywords' => '',
            'meta_description' => '',
        ]);
        DB::table('pages')->insert([
            'name' => 'Галерея',
            'slug' => 'gallery',
            'type' => 'main',
            'body' => '',
            'additional_body' => '',
            'meta_title' => 'Галерея',
            'meta_keywords' => '',
            'meta_description' => '',
        ]);
        DB::table('pages')->insert([
            'name' => 'Видео',
            'slug' => 'video',
            'type' => 'main',
            'body' => '',
            'additional_body' => '',
            'meta_title' => 'Видео',
            'meta_keywords' => '',
            'meta_description' => '',
        ]);
        DB::table('pages')->insert([
            'name' => 'Музыка',
            'slug' => 'music',
            'type' => 'main',
            'body' => '',
            'additional_body' => '',
            'meta_title' => 'Музыка',
            'meta_keywords' => '',
            'meta_description' => '',
        ]);
        DB::table('pages')->insert([
            'name' => 'Контакты',
            'slug' => 'contacts',
            'type' => 'main',
            'body' => '',
            'additional_body' => '',
            'meta_title' => 'Контакты',
            'meta_keywords' => '',
            'meta_description' => '',
        ]);
    }
}
