<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Dev',
            'email' => "dev@kashpurenko.loc",
            'password' => bcrypt("q1a1z1w2s2x2"),
        ]);
        User::create([
            'name' => 'A2-lab',
            'email' => "dev@a2-lab.com",
            'password' => bcrypt("q1a1z1w2s2x2"),
        ]);
    }
}
