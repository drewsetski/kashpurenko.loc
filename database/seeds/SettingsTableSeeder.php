<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'email'         => 'test@a2-lab.com',
            'logo_title'    => 'Владислав Кашпуренко',
            'logo_slogan'   => 'Народный артист Украины',
            'slogan_h1'     => 'Первый заголовок слогана',
            'slogan_h2'     => 'Второй заголовок слогана',
            'slogan_text'   => 'Текст слогана',
        ]);
    }
}
