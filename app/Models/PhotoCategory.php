<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhotoCategory extends Model
{
    protected $table = 'photo_categories';

    protected $guarded = ['id'];

    protected $casts = [
        'meta_keywords' => 'array'
    ];

    /**
     * Photo category can have many photos
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }
}
