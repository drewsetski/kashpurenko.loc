<?php

namespace App\Models;

use File;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    const PAGE_TYPE_MAIN = 'main';
    const PAGE_TYPE_OTHER = 'other';
    const PAGE_IMAGES_FOLDER = 'uploads/pages/';

    protected $table = 'pages';

    protected $guarded = ['id'];

    protected $casts = [
        'meta_keywords' => 'array'
    ];

    /**
     * Find page by:
     *  - name
     *  - link
     *  - meta_title
     *  - meta_keywords
     *  - meta_description
     * @param $query
     * @param $search
     * @return mixed
     */
    public function scopeSearch($query, $search)
    {
        return $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('slug', 'like', '%' . $search . '%')
                    ->orWhere('meta_title', 'like', '%' . $search . '%')
                    ->orWhere('meta_keywords', 'like', '%' . $search . '%')
                    ->orWhere('meta_description', 'like', '%' . $search . '%');
    }

    public static function saveImage($file, $id, $type, $update = false)
    {
        self::checkDirectory(self::PAGE_IMAGES_FOLDER);
        $page = self::find( $id );
        if($update) {
            $old_filename = $page->getAttribute($type);
            File::delete( self::PAGE_IMAGES_FOLDER . $old_filename );
        }
        $filename  = uniqid() . '.' . $file->getClientOriginalExtension();

        $file->move(self::PAGE_IMAGES_FOLDER, $filename);

        self::find( $id )->update([$type => $filename]);
    }

    /**
     * Check folder existence
     *
     * @param $dir
     */
    public static function checkDirectory($dir)
    {
        if (! File::isDirectory($dir)) {
            File::makeDirectory($dir, 493, true);
        }
    }
}
