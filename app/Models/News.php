<?php

namespace App\Models;

use App\Helpers\DateFormat;
use Carbon\Carbon;
use File;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $guarded = [
        'id'
    ];

    protected $casts = [
        'meta_keywords' => 'array'
    ];

    protected $appends = [
        'date',
        'human_date'
    ];

    public function getDateAttribute()
    {
        Carbon::setLocale(config('app.locale'));
        $date = Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['created_at'])->toDateString();
        return $date;
    }

    public function getHumanDateAttribute()
    {
        return DateFormat::post($this->attributes['created_at']);
    }
    
    /**
     * Find article by:
     *  - name
     *  - slug
     *  - description
     *  - body
     *  - meta_title
     *  - meta_keywords
     *  - meta_description
     * @param $query
     * @param $search
     * @return mixed
     */
    public function scopeSearch($query, $search)
    {
        return $query->where('name', 'like', '%' . $search . '%')
            ->orWhere('slug', 'like', '%' . $search . '%')
            ->orWhere('description', 'like', '%' . $search . '%')
            ->orWhere('body', 'like', '%' . $search . '%')
            ->orWhere('meta_title', 'like', '%' . $search . '%')
            ->orWhere('meta_keywords', 'like', '%' . $search . '%')
            ->orWhere('meta_description', 'like', '%' . $search . '%');
    }

    /**
     * Upload image file
     *
     * @param $file
     * @param integer $id
     * @param bool $update
     */
    public static function saveFile($file, $id, $update = false)
    {
        $article = self::find( $id );
        $dir = 'uploads/news/';
        $name = 'article';
        if($update) {
            $old_filename = $article->image;
            File::delete( $dir . $old_filename );
        }
        $filename  = $name . '_' . time() . '.' . $file->getClientOriginalExtension();

        $file->move($dir, $filename);

        self::find( $id )->update(['image' => $filename]);
    }

    /**
     * Destroy ad
     *
     * @param array|int $id
     * @return int|void
     */
    public static function destroy($id)
    {
        $article = self::find( $id );
        $dir =  'uploads/news/';
        File::delete( $dir . $article->image );

        parent::destroy( $id );
    }
}
