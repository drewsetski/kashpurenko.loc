<?php

namespace App\Models;

use File;
use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = 'settings';

    protected $guarded = ['id'];
    public $timestamps = false;

    const LOGO_FOLDER = 'uploads/logo/';

    public static function saveImage($file)
    {
        Page::checkDirectory(self::LOGO_FOLDER);
        $settings = self::first();
        $old_filename = $settings->getAttribute('logo');
        if(File::exists(self::LOGO_FOLDER . $old_filename))
            File::delete( self::LOGO_FOLDER . $old_filename );

        $filename  = uniqid() . '.' . $file->getClientOriginalExtension();

        $file->move(self::LOGO_FOLDER, $filename);

        self::first()->update(['logo' => $filename]);
    }
}
