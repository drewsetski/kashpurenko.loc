<?php

namespace App\Models;

use File;
use Image;
use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $table = 'slides';
    protected $fillable = ['image'];

    /**
     * Create slide
     *
     * @param $image
     */
    public static function saveSlide( $image ){

        $filename  = uniqid() . '.' . $image->getClientOriginalExtension();

        $img = Image::make($image->getRealPath());
        $img->heighten(968)->save( public_path('uploads/slides/' . $filename) );

        self::create(['image' => $filename]);
    }

    /**
     * Remove slides
     *
     * @param array|int $ids
     * @return int|void
     */
    public static function destroy( $ids ){

        $slides = self::whereIn('id', $ids)->get();

        foreach( $slides as $slide )
            File::delete( public_path('uploads/slides/'.$slide->image) );

        parent::destroy( $ids );
    }
}
