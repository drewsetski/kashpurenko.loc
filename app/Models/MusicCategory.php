<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MusicCategory extends Model
{
    protected $table = 'music_categories';

    protected $guarded = ['id'];

    protected $casts = [
        'meta_keywords' => 'array'
    ];

    /**
     * Music category can have many musics
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function musics()
    {
        return $this->hasMany(Music::class);
    }
}
