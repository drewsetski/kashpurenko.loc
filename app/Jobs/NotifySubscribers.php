<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\News;
use App\Models\Subscriber;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class NotifySubscribers extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $article;

    /**
     * Create a new job instance.
     *
     * @param News $article
     */
    public function __construct(News $article)
    {
        $this->article = $article;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $emails = Subscriber::pluck('email')->toArray();
        $settings = Settings::first();
        foreach ($emails as $email) {
            Mail::send('emails.newsletter', ['article' => $this->article], function ($message) use ($email, $settings) {
                $message->from($settings->email, $settings->logo_title);
                $message->to($email);
            });
        }
    }
}
