<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Page;

class PageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name' => 'required',
                    'slug' => 'required|unique:pages,slug',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                if($this->page->type == 'other') {
                    return [
                        'name' => 'required',
                        'slug' => 'required|unique:pages,slug,' . $this->page->id,
                    ];
                } else {
                    return [
                        'name' => 'required',
                        'slug' => 'unique:pages,slug,' . $this->page->id,
                    ];
                }
            }
            default:break;
        }
    }
}
