<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Contact;
use App\Models\News;
use App\Models\Page;
use App\Models\Music;
use App\Models\MusicCategory;
use App\Models\PhotoCategory;
use App\Models\Settings;
use App\Models\Slide;
use App\Models\Social;
use App\Models\Subscriber;
use App\Models\Video;
use Illuminate\Http\Request;
use Mail;

class HomeController extends Controller
{
    public $lastSixNewsClasses = [
        'fadeInLeft',
        'fadeInDown',
        'fadeInRight',
        'fadeInLeft',
        'fadeInUp',
        'fadeInRight'
    ];
    public $newsClasses = [
        'fadeInLeft',
        'fadeInRight',
        'fadeInLeft',
        'fadeInRight',
        'fadeInLeft',
        'fadeInRight'
    ];

    public function __construct()
    {
        view()->share([
            'socials' => Social::all(),
            'pages' => Page::whereType('main')->orderBy('id')->get(),
            'other_pages' => Page::whereType('other')->orderBy('id')->get(),
            'settings' => Settings::first()
        ]);
    }
    /**
     * Display index page
     */
    public function index()
    {
        $page = Page::whereSlug('index')->firstOrFail();
        $music = Music::take(9)->with('category')->get();
        $news = News::take(6)->orderBy('created_at', 'desc')->get();
        $slides = Slide::all();
        $wowClasses = $this->lastSixNewsClasses;

        return view('home', compact(
            'page',
            'slides',
            'music',
            'news',
            'wowClasses'
        ));
    }

    /**
     * Display biography page
     */
    public function biography()
    {
        $page = Page::whereSlug('biography')->firstOrFail();

        return view('biography', compact(
            'page'
        ));
    }

    /**
     * Display gallery page
     */
    public function gallery()
    {
        $page = Page::whereSlug('gallery')->firstOrFail();

        return view('gallery', [
            'page' => $page,
            'categories' => PhotoCategory::all()
        ]);
    }

    /**
     * Display all photos related to specified category
     *
     * @param $link
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function photos($link)
    {
        $category = PhotoCategory::whereLink($link)->first();
        $page = Page::whereSlug('gallery')->firstOrFail();
        return view('photos', [
            'title' => $category->name,
            'meta_keywords' => $category->meta_keywords,
            'meta_description' => $category->meta_description,
            'photos' => $category->photos,
            'main_bg' => $page->main_bg,
            'main_color' => $page->main_color
        ]);
    }

    /**
     * Display news page
     */
    public function news()
    {
        $page = Page::whereSlug('news')->firstOrFail();
        $news = News::orderBy('created_at', 'desc')->paginate(4);
        $wowClasses = $this->newsClasses;

        return view('news', compact(
            'page',
            'news',
            'wowClasses'
        ));
    }

    /**
     * Display article
     *
     * @param $article_slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function article( $article_slug )
    {
        $page = Page::whereSlug('news')->firstOrFail();
        $article = News::whereSlug($article_slug)->firstOrFail();
        return view('article', [
            'title' => $article->name,
            'meta_keywords' => $article->meta_keywords,
            'meta_description' => $article->meta_description,
            'article' => $article,
            'main_bg' => $page->main_bg,
            'main_color' => $page->main_color
        ]);
    }

    /**
     * Display video page
     */
    public function video()
    {
        $page = Page::whereSlug('video')->firstOrFail();
        $videos = Video::orderBy('created_at', 'desc')->paginate(4);
        $wowClasses = $this->newsClasses;

        return view('videos', compact(
            'page',
            'videos',
            'wowClasses'
        ));
    }

    /**
     * Display gallery page
     */
    public function contacts()
    {
        $page = Page::whereSlug('contacts')->firstOrFail();
        $contacts = Contact::all();

        return view('contacts', compact(
            'page',
            'contacts'
        ));
    }

    /**
     * Display music categories
     */
    public function music()
    {
        $page = Page::whereSlug('music')->firstOrFail();
        $categories = MusicCategory::all();

        return view('music', compact(
            'page',
            'categories'
        ));
    }

    /**
     * Display all songs related to specified category
     *
     * @param $link
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function songs($link)
    {
        $page = Page::whereSlug('music')->firstOrFail();
        $category = MusicCategory::whereLink($link)->first();
        return view('songs', [
            'title' => $category->name,
            'meta_keywords' => $category->meta_keywords,
            'meta_description' => $category->meta_description,
            'musics' => $category->musics,
            'main_bg' => $page->main_bg,
            'main_color' => $page->main_color
        ]);
    }

    public function sendFeedback(Request $request)
    {
        try {
            $settings = Settings::first();
            $unserialized = $request->except('_token', 'stripHTML');
            Mail::send('emails.feedback', [
                'unserialized' => $unserialized
            ], function ($message) use ($settings, $request) {
                $message->from($request->input('email'), $request->input('name'));
                $message->to($settings->email, $settings->logo_title)->subject(trans('labels.message_from', ['name' => $request->input('name')]));
            });
            return 'success';
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Handling subscribing to newsletter request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribe(Request $request)
    {
        Subscriber::updateOrCreate($request->only('email'));
        return response()->json('success');
    }

    public function page($page_url)
    {
        $page = Page::whereSlug( $page_url )->firstOrFail();
        return view('page', compact(
                    'page'
                ));
    }
}
