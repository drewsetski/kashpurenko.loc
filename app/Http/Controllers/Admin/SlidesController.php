<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slide;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SlidesController extends Controller
{
    /**
     * Display a listing of the slides.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slide::orderBy('id', 'desc')->get();
        $title = trans('labels.slides.slides');
        return view('admin.slides.index', compact(
            'slides',
            'title'
        ));
    }

    /**
     * Save slides
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        foreach ($request->file('image') as $image) {
            Slide::saveSlide($image);
        }

        Flash::success(trans('messages.created.slides'));
        return redirect()->route('admin.slides.index');
    }

    /**
     * Delete all checked slides
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        if( $request->get('action') == 'delete' ) {
            $ids = $request->get('check');
            Slide::destroy( $ids );
        }
        Flash::success(trans('messages.deleted.slides'));
        return redirect()->route('admin.slides.index');
    }
}
