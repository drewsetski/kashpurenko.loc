<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NewsRequest;
use App\Jobs\NotifySubscribers;
use App\Models\News;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news_count = News::count();
        $title = trans('labels.news.news');
        return view('admin.news.index', compact(
            'news_count',
            'title'
        ));
    }

    /**
     *  Return news to bootstrap table with search and pagination
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        $offset = request()->get('offset');
        $limit = request()->get('limit');
        $search = request()->get('search');

        if ($search) {
            $rows = News::search($search)->skip($offset)->take($limit)->get();
            $total = count($rows);
        } else {
            $rows = News::skip($offset)->take($limit)->get();
            $total = News::count();
        }

        return response()->json(compact('total', 'rows'));
    }

    /**
     * Show the form for creating a new article
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $article = new News;
        $title = trans('labels.news.creating');
        $route = route('admin.news.store');
        $method = 'POST';

        return view('admin.news.edit', compact(
            'article',
            'route',
            'title',
            'method'
        ));
    }

    /**
     * Store a newly created article in database.
     *
     * @param NewsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(NewsRequest $request)
    {
        $article = News::create($request->except('image'));
        if( $file = $request->file('image') ) {
            News::saveFile($file, $article->getAttribute('id'), false);
        }
        $this->dispatch(new NotifySubscribers($article));
        Flash::success(trans('messages.created.article', ['name' => $article->getAttribute('name')]));
        return redirect()->route('admin.news.index');
    }

    /**
     * Show the form for editing the specified article.
     *
     * @param News $article
     * @return \Illuminate\Http\Response
     */
    public function edit(News $article)
    {
        $title = trans('labels.news.editing');

        $route = route('admin.news.update', array('article' => $article));
        $method = 'PUT';

        return view('admin.news.edit', compact(
            'article',
            'route',
            'title',
            'method'
        ));
    }

    /**
     * Update the specified article in storage.
     *
     * @param News $article
     * @param NewsRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(News $article, NewsRequest $request)
    {
        $article->update(array_merge($request->except('image', 'meta_keywords'),
            array(
                'meta_keywords' => $request->input('meta_keywords')
            )
            ));

        if( $file = $request->file('image') ) {
            News::saveFile($file, $article->getAttribute('id'), true);
        }

        Flash::success(trans('messages.updated.article', ['name' => $article->getAttribute('name')]));
        return redirect()->route('admin.news.edit', ['article' => $article]);
    }

    /**
     * Remove the specified article from storage.
     *
     * @param News $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $article)
    {
        News::destroy($article->getAttribute('id'));
    }
}
