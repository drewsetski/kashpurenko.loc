<?php

namespace App\Http\Controllers\Admin;

use App\Models\Social;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

class SocialsController extends Controller
{
    // allowed social networks
    public $socials = [
        'vk'            => 'VK'        ,
        'youtube'       => 'YouTube'   ,
        'twitter'       => 'Twitter'   ,
        'facebook'      => 'Facebook'  ,
        'google-plus'   => 'Google+'   ,
        'instagram'     => 'Instagram'
    ];

    /**
     * Display index page for socials
     * 
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.socials.index', ['title' => trans('labels.socials'), 'socials' => $this->socials]);
    }

    /**
     * Return all socials in json
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSocials()
    {
        return response()->json(Social::all());
    }

    /**
     * Create new social
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Request $request)
    {
        Social::create([
            'key'   => $request->input('social'),
            'value' => $request->input('link')
        ]);
        Flash::success(trans('labels.successfully_added'));
        return redirect()->back();
    }

    /**
     * Delete social
     *
     * @param Social $social
     * @throws \Exception
     */
    public function destroy(Social $social)
    {
        $social->delete();
    }

    /**
     * Display edit page for specified social
     *
     * @param Social $social
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Social $social)
    {
        return view('admin.socials.edit', [
            'social' => $social, 'socials' => $this->socials, 'title' => trans('labels.editing')
        ]);
    }

    /**
     * Update specified social
     *
     * @param Social $social
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Social $social, Request $request)
    {
        $social->update([
            'key'   => $request->input('social'),
            'value' => $request->input('link')
        ]);
        Flash::success(trans('labels.successfully_edited'));
        return redirect('master/socials');
    }
}
