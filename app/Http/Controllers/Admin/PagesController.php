<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PageRequest;
use App\Models\Page;
use File;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages_count = Page::count();
        $title = trans('labels.pages.pages');
        return view('admin.pages.index', compact(
            'pages_count', 
            'title'
        ));
    }

    /**
     *  Return pages to bootstrap table with search and pagination
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        $offset = request()->get('offset');
        $limit = request()->get('limit');
        $search = request()->get('search');

        if ($search) {
            $rows = Page::search($search)->skip($offset)->take($limit)->get();
            $total = count($rows);
        } else {
            $rows = Page::skip($offset)->take($limit)->get();
            $total = Page::count();
        }

        return response()->json(compact('total', 'rows'));
    }

    /**
     * Show the form for creating a new page.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = new Page;
        $title = trans('labels.pages.creating');
        $route = route('admin.pages.store');
        $method = 'POST';

        return view('admin.pages.edit', compact(
            'page',
            'route',
            'title',
            'method'
        ));
    }

    /**
     * Store a newly created page in database.
     *
     * @param PageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PageRequest $request)
    {
        $page = Page::create(array_merge(
            $request->except('main_bg', 'additional_bg'),
            [
                'type' => Page::PAGE_TYPE_OTHER
            ]
        ));
        if($file = $request->file('main_bg')) {
            Page::saveImage($file, $page->id, 'main_bg', false);
        }
        if($file = $request->file('additional_bg')) {
            Page::saveImage($file, $page->id, 'additional_bg', false);
        }
        
        Flash::success(trans('messages.created.page', ['name' => $page->getAttribute('name')]));
        return redirect()->route('admin.pages.index');
    }

    /**
     * Show the form for editing the specified page.
     *
     * @param Page $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        $title = trans('labels.pages.editing');
        $route = route('admin.pages.update', array('page' => $page));
        $method = 'PUT';

        return view('admin.pages.edit', compact(
            'page',
            'route',
            'title',
            'method'
        ));
    }

    /**
     * Update the specified page in storage.
     *
     * @param Page $page
     * @param PageRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Page $page, PageRequest $request)
    {
        $page->update(array_merge(
            $request->except('main_bg', 'additional_bg')
        ));
        if($file = $request->file('main_bg')) {
            Page::saveImage($file, $page->id, 'main_bg', false);
        }
        if($file = $request->file('additional_bg')) {
            Page::saveImage($file, $page->id, 'additional_bg', false);
        }

        Flash::success(trans('messages.updated.page', ['name' => $page->getAttribute('name')]));
        return redirect()->route('admin.pages.edit', ['page' => $page]);
    }

    /**
     * Remove the specified page from storage.
     *
     * @param Page $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        File::delete( Page::PAGE_IMAGES_FOLDER . $page->main_bg);
        File::delete( Page::PAGE_IMAGES_FOLDER . $page->additional_bg);
        $page->delete();
    }
}
