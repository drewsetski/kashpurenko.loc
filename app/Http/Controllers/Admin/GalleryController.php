<?php

namespace App\Http\Controllers\Admin;

use App\Models\Photo;
use App\Models\PhotoCategory;
use File;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class GalleryController extends Controller
{
    const CATEGORIES_PATH = 'uploads/categories/';
    const PHOTOS_PATH = 'uploads/photos/';

    /**
     * Display all gallery categories
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function categories()
    {
        $categories = PhotoCategory::all();
        return view('admin.gallery.categories.index', [
            'title' => trans('labels.gallery.categories'),
            'categories' => $categories
        ]);
    }

    /**
     * Display page for creating new category
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addCategory()
    {
        return view('admin.gallery.categories.add', [
            'title' => trans('labels.gallery.add_category'),
            'category' => null
        ]);        
    }

    /**
     * Create new category
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createCategory(Request $request)
    {
        $filename = $this->uploadFile($request->file('image'), self::CATEGORIES_PATH);
        PhotoCategory::create([
            'name'              => $request->input('name'),
            'image'             => $filename,
            'link'              => $request->input('link'),
            'meta_title'        => $request->input('meta_title'),
            'meta_keywords'     => $request->input('meta_keywords'),
            'meta_description'  => $request->input('meta_description'),
        ]);
        Flash::success(trans('labels.successfully_added'));
        return redirect('master/gallery/categories');
    }

    /**
     * Display edit page for specified category
     *
     * @param PhotoCategory $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editCategory(PhotoCategory $category)
    {
        return view('admin.gallery.categories.add', ['category' => $category, 'title' => trans('labels.editing')]);
    }

    /**
     * Update existing category
     *
     * @param Request $request
     * @param PhotoCategory $category
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateCategory(Request $request, PhotoCategory $category)
    {
        if($request->file('image')) {
            $filename = $this->uploadFile($request->file('image'), self::CATEGORIES_PATH);
            $category->update([
                'image'             => $filename,
            ]);
        }
        $category->update([
            'name'              => $request->input('name'),
            'link'              => $request->input('link'),
            'meta_title'        => $request->input('meta_title'),
            'meta_keywords'     => $request->input('meta_keywords'),
            'meta_description'  => $request->input('meta_description'),
        ]);
        Flash::success(trans('labels.successfully_edited'));
        return redirect('master/gallery/categories');
    }

    /**
     * Delete specified category
     *
     * @param PhotoCategory $category
     * @return string
     * @throws \Exception
     */
    public function deleteCategory(PhotoCategory $category)
    {
        $category->delete();
        return 'true';
    }

    /**
     * Display all photos
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function photos()
    {
        $categories = PhotoCategory::all();
        return view('admin.gallery.photos.index', ['title' => trans('labels.gallery.photos'), 'categories' => $categories]);
    }

    /**
     * Display all photos related to specified category
     *
     * @param PhotoCategory $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function photosByCategory(PhotoCategory $category)
    {
        return view('admin.gallery.photos.photos', [
            'title' => trans('labels.gallery.photos'), 'photos' => $category->photos, 'category' => $category
        ]);
    }

    /**
     * Display page for creating new photo
     *
     * @param PhotoCategory $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addPhoto(PhotoCategory $category)
    {
        return view('admin.gallery.photos.add', [
            'title' => trans('labels.gallery.add_photo'),
            'category' => $category,
            'photo' => null
        ]);
    }

    /**
     * Display page for editing existing photo
     *
     * @param PhotoCategory $category
     * @param Photo $photo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editPhoto(PhotoCategory $category, Photo $photo)
    {
        return view('admin.gallery.photos.add', [
            'title' => trans('labels.editing'),
            'category' => $category,
            'photo' => $photo
        ]);
    }

    /**
     * Update existing photo
     *
     * @param PhotoCategory $category
     * @param Photo $photo
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updatePhoto(PhotoCategory $category, Photo $photo, Request $request)
    {
        if($request->file('image')[0]) {
            $filename = $this->uploadFile($request->file('image')[0], self::PHOTOS_PATH);
            $photo->update([
                'file'         => $filename, 
            ]);
        }
        $photo->update([
            'name'          => $request->input('name'),
            'description'   => $request->input('description'),
        ]);
        Flash::success(trans('labels.successfully_edited'));
        return redirect('master/gallery/photos/' . $category->id);
    }

    /**
     * Create new photo
     *
     * @param PhotoCategory $category
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createPhoto(PhotoCategory $category, Request $request)
    {
        foreach ($request->file('image') as $file) {
            $filename = $this->uploadFile($file, self::PHOTOS_PATH);
            $photo = new Photo([
                'file' => $filename
            ]);
            $category->photos()->save($photo);
        }
        Flash::success(trans('labels.successfully_added'));
        return redirect('master/gallery/photos/' . $category->id);
    }

    /**
     * Delete specified photos
     *
     * @param PhotoCategory $category
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deletePhotos(PhotoCategory $category, Request $request)
    {
        $photos = $request->input('check');
        foreach ($photos as $photo) {
            $file = Photo::find($photo);
            File::delete(public_path(self::PHOTOS_PATH . $file->file));
            $file->delete();
        }
        Flash::success(trans('labels.successfully_deleted'));
        return redirect('master/gallery/photos/' . $category->id);
    }

    /**
     * Upload file to server
     *
     * @param UploadedFile $file
     * @param $path
     * @param PhotoCategory|null $category
     * @return null|string
     * @internal param Request $request
     */
    public function uploadFile(UploadedFile $file, $path, PhotoCategory $category = null)
    {
        if ($category !== null)
            File::delete(public_path($path . $category->image));
        // if category image is set
        if ($file) {
            $filename = uniqid() . "." . $file->getClientOriginalExtension();
            $file->move(public_path($path), $filename);
            return $filename;
        }
        return null;
    }
}
