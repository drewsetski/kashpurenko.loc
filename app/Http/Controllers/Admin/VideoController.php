<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VideoRequest;
use App\Models\Video;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    /**
     * Display a listing of the videos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::orderBy('id', 'desc')->get();
        $title = trans('labels.videos.videos');
        return view('admin.videos.index', compact(
            'videos',
            'title'
        ));
    }

    /**
     *  Post actions to index page:
     *      - delete
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postIndex(Request $request)
    {
        if( $request->get('action') == 'delete' ){
            $ids = $request->get('check');
            Video::destroy($ids);
        }

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $video = new Video;
        $title = trans('labels.videos.creating');
        $route = route('admin.videos.store');
        $method = 'POST';

        return view('admin.videos.edit', compact(
            'video',
            'route',
            'title',
            'method'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param VideoRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoRequest $request)
    {
        $video = Video::create($request->all());

        Flash::success(trans('messages.created.video', ['name' => $video->getAttribute('name')]));
        return redirect()->route('admin.videos.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Video $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        $title = trans('labels.videos.editing');
        $route = route('admin.videos.update', array('video' => $video));
        $method = 'PUT';

        return view('admin.videos.edit', compact(
            'video',
            'route',
            'title',
            'method'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Video $video
     * @param VideoRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Video $video, VideoRequest $request)
    {
        $video->update($request->all());

        Flash::success(trans('messages.updated.video', ['name' => $video->getAttribute('name')]));
        return redirect()->route('admin.videos.edit', ['video' => $video]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Video $video
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Video $video)
    {
        $video->delete();
    }
}
