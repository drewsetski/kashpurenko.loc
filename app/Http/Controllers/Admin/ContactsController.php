<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contact;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts_count = Contact::count();

        $title = trans('labels.contacts.contacts');
        return view('admin.contacts.index', compact(
            'contacts_count',
            'title'
        ));
    }

    /**
     *  Return contacts to bootstrap table with search and pagination
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        $offset = request()->get('offset');
        $limit = request()->get('limit');
        $search = request()->get('search');

        if ($search) {
            $rows = Contact::search($search)->skip($offset)->take($limit)->get();
            $total = count($rows);
        } else {
            $rows = Contact::skip($offset)->take($limit)->get();
            $total = Contact::count();
        }

        return response()->json(compact('total', 'rows'));
    }

    /**
     * Show the form for creating a new contact.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $contact = new Contact;
        $title = trans('labels.contacts.creating');
        $route = route('admin.contacts.store');
        $method = 'POST';

        return view('admin.contacts.edit', compact(
            'contact',
            'route',
            'title',
            'method'
        ));
    }

    /**
     * Store a newly created contact in database.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $contact = Contact::create($request->all());

        Flash::success(trans('messages.created.contact', ['name' => $contact->getAttribute('name')]));
        return redirect()->route('admin.contacts.index');
    }

    /**
     * Show the form for editing the specified contact.
     *
     * @param Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        $title = trans('labels.contacts.editing');
        $route = route('admin.contacts.update', array('contact' => $contact));
        $method = 'PUT';

        return view('admin.contacts.edit', compact(
            'contact',
            'route',
            'title',
            'method'
        ));
    }

    /**
     * Update the specified contact in storage.
     *
     * @param Contact $contact
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Contact $contact, Request $request)
    {
        $contact->update($request->all());

        Flash::success(trans('messages.updated.contact', ['name' => $contact->getAttribute('name')]));
        return redirect()->route('admin.contacts.edit', ['contact' => $contact]);
    }

    /**
     * Remove the specified contact from storage.
     *
     * @param Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();
    }
}
