<?php

namespace App\Http\Controllers\Admin;

use App\Models\Settings;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Settings::first();

        $title = trans('labels.settings');

        return view('admin.settings.index', compact(
            'settings',
            'title'
        ));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $settings = Settings::first();
        $settings->update($request->except('image'));
        if($file = $request->file('logo'))
            Settings::saveImage($file);

        Flash::success(trans('messages.updated.settings'));

        return redirect()->route('admin.settings.index');
    }
}
