<?php

namespace App\Http\Controllers\Admin;

use App\Models\Music;
use App\Models\MusicCategory;
use App\Models\PhotoCategory;
use File;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MusicController extends Controller
{
    const MUSIC_PATH = 'uploads/music/';

    /**
     * Display all music categories
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function categories()
    {
        $categories = MusicCategory::all();
        return view('admin.music.categories.index', [
            'title' => trans('labels.gallery.categories'),
            'categories' => $categories
        ]);
    }

    /**
     * Display page for creating new category
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addCategory()
    {
        return view('admin.music.categories.add', [
            'title' => trans('labels.gallery.add_category'),
            'category' => null
        ]);
    }

    /**
     * Create new category
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createCategory(Request $request)
    {
        MusicCategory::create([
            'name'              => $request->input('name'),
            'link'              => $request->input('link'),
            'meta_title'        => $request->input('meta_title'),
            'meta_keywords'     => $request->input('meta_keywords'),
            'meta_description'  => $request->input('meta_description'),
        ]);
        Flash::success(trans('labels.successfully_added'));
        return redirect('master/music/categories');
    }

    /**
     * Display edit page for specified category
     *
     * @param MusicCategory $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editCategory(MusicCategory $category)
    {
        return view('admin.music.categories.add', ['category' => $category, 'title' => trans('labels.editing')]);
    }

    /**
     * Update existing category
     *
     * @param Request $request
     * @param MusicCategory $category
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateCategory(Request $request, MusicCategory $category)
    {
        $category->update([
            'name'              => $request->input('name'),
            'link'              => $request->input('link'),
            'meta_title'        => $request->input('meta_title'),
            'meta_keywords'     => $request->input('meta_keywords'),
            'meta_description'  => $request->input('meta_description'),
        ]);
        Flash::success(trans('labels.successfully_edited'));
        return redirect('master/music/categories');
    }

    /**
     * Delete specified category
     *
     * @param MusicCategory $category
     * @return string
     * @throws \Exception
     */
    public function deleteCategory(MusicCategory $category)
    {
        $category->delete();
        return 'true';
    }

    /**
     * Display all musics
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function musics()
    {
        $categories = MusicCategory::all();
        return view('admin.music.musics.index', ['title' => trans('labels.music.musics'), 'categories' => $categories]);
    }

    /**
     * Display all musics related to specified category
     *
     * @param MusicCategory $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function musicsByCategory(MusicCategory $category)
    {
        return view('admin.music.musics.musics', [
            'title' => trans('labels.music.musics'), 'musics' => $category->musics, 'category' => $category
        ]);
    }

    /**
     * Display page for creating new music
     *
     * @param MusicCategory $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addMusic(MusicCategory $category)
    {
        return view('admin.music.musics.add', [
            'title' => trans('labels.music.add_music'),
            'category' => $category,
            'music' => null
        ]);
    }

    /**
     * Display page for editing existing music
     *
     * @param MusicCategory $category
     * @param Music $music
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editMusic(MusicCategory $category, Music $music)
    {
        return view('admin.music.musics.add', [
            'title' => trans('labels.editing'),
            'category' => $category,
            'music' => $music
        ]);
    }

    /**
     * Update existing music
     *
     * @param MusicCategory $category
     * @param Music $music
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateMusic(MusicCategory $category, Music $music, Request $request)
    {
        if($request->file('image')[0]) {
            $filename = $this->uploadFile($request->file('image')[0], self::MUSIC_PATH);
            $music->update([
                'file' => $filename,
            ]);
        }
        $music->update([
            'name' => $request->input('name'),
        ]);
        Flash::success(trans('labels.successfully_edited'));
        return redirect('master/music/musics/' . $category->id);
    }

    /**
     * Create new music
     *
     * @param MusicCategory $category
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createMusic(MusicCategory $category, Request $request)
    {
        if($request->file('image')) {
            foreach ($request->file('image') as $file) {
                $filename = $this->uploadFile($file, self::MUSIC_PATH);
                $music = new Music([
                    'name' => str_replace(['.mp3', '.wav', '.flac', '.wma'], '', $file->getClientOriginalName()),
                    'file' => $filename
                ]);
                $category->musics()->save($music);
            }
        }
        Flash::success(trans('labels.successfully_added'));
        return redirect('master/music/musics/' . $category->id);
    }

    /**
     * Delete specified musics
     *
     * @param MusicCategory $category
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteMusic(MusicCategory $category, Request $request)
    {
        $musics = $request->input('check');
        foreach ($musics as $music) {
            $file = Music::find($music);
            File::delete(public_path(self::MUSIC_PATH . $file->file));
            $file->delete();
        }
        Flash::success(trans('labels.successfully_deleted'));
        return redirect('master/music/musics/' . $category->id);
    }

    /**
     * Upload file to server
     *
     * @param UploadedFile $file
     * @param $path
     * @param PhotoCategory|null $category
     * @return null|string
     * @internal param Request $request
     */
    public function uploadFile(UploadedFile $file, $path, PhotoCategory $category = null)
    {
        if ($category !== null)
            File::delete(public_path($path . $category->image));
        // if category image is set
        if ($file) {
            $filename = uniqid() . "." . $file->getClientOriginalExtension();
            $file->move(public_path($path), $filename);
            return $filename;
        }
        return null;
    }
}
