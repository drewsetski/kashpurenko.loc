<?php

Route::pattern('id', '[0-9]+');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * Admin routes
 */
$this->get('login', 'Admin\Auth\AuthController@showLoginForm');
$this->post('login', 'Admin\Auth\AuthController@login');
$this->get('logout', 'Admin\Auth\AuthController@logout');
Route::group(['prefix' => 'master', 'namespace' => 'Admin'], function () {

    Route::get('/', 'HomeController@index')->name('admin.index');
    /*
     * Pages
     */
    Route::group(['prefix' => 'pages'], function () {
        Route::get('/', 'PagesController@index')->name('admin.pages.index');
        Route::get('/get', 'PagesController@get')->name('admin.pages.get');
        Route::get('/add', 'PagesController@create')->name('admin.pages.add');
        Route::post('/add', 'PagesController@store')->name('admin.pages.store');
        Route::get('/{page}', 'PagesController@edit')->name('admin.pages.edit');
        Route::put('/{page}', 'PagesController@update')->name('admin.pages.update');
        Route::delete('/{page}', 'PagesController@destroy');
    });

    /*
     * News
     */
    Route::group(['prefix' => 'news'], function () {
        Route::get('/', 'NewsController@index')->name('admin.news.index');
        Route::get('/get', 'NewsController@get')->name('admin.news.get');
        Route::get('/add', 'NewsController@create')->name('admin.news.add');
        Route::post('/add', 'NewsController@store')->name('admin.news.store');
        Route::get('/{article}', 'NewsController@edit')->name('admin.news.edit');
        Route::put('/{article}', 'NewsController@update')->name('admin.news.update');
        Route::delete('/{article}', 'NewsController@destroy');
    });

    /*
     * Videos
     */
    Route::group(['prefix' => 'videos'], function () {
        Route::get('/', 'VideoController@index')->name('admin.videos.index');
        Route::post('/', 'VideoController@postIndex')->name('admin.videos.postIndex');
        Route::get('/get', 'VideoController@get')->name('admin.videos.get');
        Route::get('/add', 'VideoController@create')->name('admin.videos.add');
        Route::post('/add', 'VideoController@store')->name('admin.videos.store');
        Route::get('/{video}', 'VideoController@edit')->name('admin.videos.edit');
        Route::put('/{video}', 'VideoController@update')->name('admin.videos.update');
        Route::delete('/{video}', 'VideoController@destroy');
    });

    /*
     * Socials
     */
    Route::group(['prefix' => 'socials'], function () {
        Route::get('/', 'SocialsController@index')->name('socials.index');
        Route::post('/', 'SocialsController@add');
        Route::delete('{social}', 'SocialsController@destroy');
        Route::get('getSocials', 'SocialsController@getSocials');
        Route::get('{social}', 'SocialsController@edit');
        Route::put('{social}', 'SocialsController@update');
    });

    /*
     * Gallery
     */
    Route::group(['prefix' => 'gallery'], function () {
        Route::group(['prefix' => 'categories'], function () {
            Route::get('/', 'GalleryController@categories')->name('gallery.categories');
            Route::post('/', 'GalleryController@createCategory');
            Route::get('add', 'GalleryController@addCategory');
            Route::get('edit/{category}', 'GalleryController@editCategory');
            Route::put('edit/{category}', 'GalleryController@updateCategory');
            Route::delete('{category}', 'GalleryController@deleteCategory');
        });
        Route::group(['prefix' => 'photos'], function () {
            Route::get('/', 'GalleryController@photos')->name('gallery.photos');
            Route::get('{category}', 'GalleryController@photosByCategory');
            Route::post('{category}', 'GalleryController@createPhoto');
            Route::get('{category}/add', 'GalleryController@addPhoto');
            Route::get('{category}/{photo}', 'GalleryController@editPhoto');
            Route::put('{category}/{photo}', 'GalleryController@updatePhoto');
            Route::delete('{category}', 'GalleryController@deletePhotos');
        });
    });

    /*
     * Music
     */
    Route::group(['prefix' => 'music'], function () {
        Route::group(['prefix' => 'categories'], function () {
            Route::get('/', 'MusicController@categories')->name('music.categories');
            Route::post('/', 'MusicController@createCategory');
            Route::get('add', 'MusicController@addCategory');
            Route::get('edit/{category}', 'MusicController@editCategory');
            Route::put('edit/{category}', 'MusicController@updateCategory');
            Route::delete('{category}', 'MusicController@deleteCategory');
        });
        Route::group(['prefix' => 'musics'], function () {
            Route::get('/', 'MusicController@musics')->name('music.musics');
            Route::get('{category}', 'MusicController@musicsByCategory');
            Route::post('{category}', 'MusicController@createMusic');
            Route::get('{category}/add', 'MusicController@addMusic');
            Route::get('{category}/{music}', 'MusicController@editMusic');
            Route::put('{category}/{music}', 'MusicController@updateMusic');
            Route::delete('{category}', 'MusicController@deleteMusic');
        });
    });
    /*
     * Settings
     */
    Route::group(['prefix' => 'settings'], function () {
        Route::get('/', 'SettingsController@index')->name('admin.settings.index');
        Route::post('update', 'SettingsController@update')->name('admin.settings.update');
    });

    /*
     * Slides
     */
    Route::group(['prefix' => 'slides'], function () {
        Route::get('/', 'SlidesController@index')->name('admin.slides.index');
        Route::post('add', 'SlidesController@store')->name('admin.slides.store');
        Route::delete('delete', 'SlidesController@delete')->name('admin.slides.delete');
    });

    /*
     * Contacts
     */
    Route::group(['prefix' => 'contacts'], function () {
        Route::get('/', 'ContactsController@index')->name('admin.contacts.index');
        Route::get('/get', 'ContactsController@get')->name('admin.contacts.get');
        Route::get('/add', 'ContactsController@create')->name('admin.contacts.add');
        Route::post('/add', 'ContactsController@store')->name('admin.contacts.store');
        Route::get('/{contact}', 'ContactsController@edit')->name('admin.contacts.edit');
        Route::put('/{contact}', 'ContactsController@update')->name('admin.contacts.update');
        Route::delete('/{contact}', 'ContactsController@destroy');
    });
});

Route::get('/', 'HomeController@index')->name('index');
Route::post('/', 'HomeController@subscribe')->name('subscribe');
Route::get('biography', 'HomeController@biography')->name('biography');
Route::get('news', 'HomeController@news')->name('news');
Route::get('news/{article}', 'HomeController@article')->name('article');
Route::get('gallery', 'HomeController@gallery')->name('gallery');
Route::get('gallery/{link}', 'HomeController@photos')->name('photos');
Route::get('music', 'HomeController@music')->name('music');
Route::get('music/{link}', 'HomeController@songs')->name('songs');
Route::get('video', 'HomeController@video')->name('video');
Route::get('contacts', 'HomeController@contacts')->name('contacts');
Route::post('feedback', 'HomeController@sendFeedback')->name('feedback');
Route::get('/{page_url}','HomeController@page');