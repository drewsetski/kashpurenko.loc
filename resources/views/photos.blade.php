@extends('app')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/jquery.fancybox.css')}}">
@endsection

@section('header')
    <header class="header__mod" @if($main_bg) style="background-image: url({{ url('uploads/pages/' . $main_bg) }})" @endif>
        @include('partials._nav')
    </header>
@endsection

@section('content')
    <main>

        <section class="well2" {{ $main_color != '' ? "style='background: $main_color" : ''}}>
            <div class="container">
                <h3 class="mod-center">{{ $title }}</h3>
                @foreach($photos->chunk(3) as $three_photos)
                    <div class="row row offset3">
                        @foreach($three_photos as $key => $photo)
                            <div class="grid_4 block4">
                                <a class="thumb" href="{{ url('uploads/photos/' . $photo->file) }}">
                                    <img src="{{ url('uploads/photos/' . $photo->file) }}" alt=""/>
                                    <span class="thumb_overlay"></span>
                                </a>
                                @if(!empty($photo->name))
                                    <h4 class="color-2 fw">{{ $photo->name }}</h4>
                                @endif
                                @if(!empty($photo->description))
                                    <p>{{ $photo->description }}</p>
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </section>
    </main>
@endsection