<!DOCTYPE html>
<html lang="en">
<head>
    @if ( isset( $page->meta_title ) )
        <title>{!! $page->meta_title !!}</title>
    @elseif ( isset( $title ) )
        <title>{!! $title !!}</title>
    @endif
    @if ( isset( $page->meta_keywords ) )
        <meta name="keywords" content="
        @foreach($page->meta_keywords as $keyword)
        {{ $keyword }}
        @endforeach">
    @elseif ( isset( $meta_keywords ) )
        <meta name="keywords" content="
        @foreach($meta_keywords as $keyword)
            {{ $keyword }}
        @endforeach">
    @endif
    @if ( isset( $page->meta_description )  )
        <meta name="description" content="{{ $page->meta_description }}">
    @elseif ( isset( $meta_description ) )
        <meta name="description" content="{{ $meta_description }}">
    @endif
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{URL::asset('images/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{URL::asset('css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/grid.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
    @yield('styles')

    <script src="{{URL::asset('js/jquery.js')}}"></script>
    <script src="{{URL::asset('js/jquery-migrate-1.2.1.js')}}"></script>
    <!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="{{URL::asset('images/ie8-panel/warning_bar_0000_us.jpg')}}" border="0" height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div>
    <script src="{{URL::asset('js/html5shiv.js')}}"></script>
    <![endif]-->

    <script src='{{URL::asset('js/device.min.js')}}'></script>

</head>

<body>
<div class="page">
    <!--========================================================
                              HEADER
    =========================================================-->
    @yield('header')

    <!--========================================================
                              CONTENT
    =========================================================-->
    @yield('content')

    <!--========================================================
                              FOOTER
    =========================================================-->
    <footer class="wow fadeInUp" data-wow-duration="2s">

        <div class="container">

            <div class="brand">
                <h1 class="brand_name">
                    <a href="{{ URL::route('index') }}">{!! preg_split("/[\s,]+/",$settings['logo_title'])[0] !!} <br> {!!  preg_split("/[\s,]+/",$settings['logo_title'])[1] !!}</a>
                </h1>
                @if(isset($settings['logo_slogan']))
                    <span class="brand_slogan">
                        {!! $settings['logo_slogan'] !!}
                    </span>
                @endif
                <p class="copyright">© <span id="copyright-year"></span><p>
            </div>

            <div class="mg-add3 mod-position2">
                <h4 class="color-1">{{ trans('labels.socials') }}</h4>

                <ul class="inline-list">
                    @foreach($socials as $social)
                        <li><a class="fa fa-{{$social->key}}" href="{{$social->value}}"></a></li>
                    @endforeach
                </ul>
            </div>
            <!-- {%FOOTER_LINK} -->
        </div>
    </footer>

</div>

<script src="{{URL::asset('js/script.js')}}"></script>
</body>

</html>