@extends('app')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/camera.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/subscribe-form.css')}}">
@endsection

@section('header')
    <header>

        <div class="camera_container">
            <div id="camera" class="camera_wrap">
                @if(count($slides)>0)
                    @foreach($slides as $slide)
                        <div data-src="{{ asset('uploads/slides/' . $slide->image) }}"></div>
                    @endforeach
                @else
                    @for($index=0; $index<3; $index++)
                        <div data-src="images/slider01.jpg"></div>
                    @endfor
                @endif
            </div>
            <div class="slogan  wow fadeIn" data-wow-duration="2s">
                <h3>
                    @if(isset($settings['slogan_h1'])){!! $settings['slogan_h1'] !!}@endif
                    @if(isset($settings['slogan_h2']))<br><span>{!! $settings['slogan_h2'] !!}</span>@endif
                </h3>
                @if(isset($settings['slogan_text']))<p>{!! $settings['slogan_text'] !!}</p>@endif
            </div>
        </div>
        @include('partials._nav')
    </header>
@endsection

@section('content')
    <main>
        <section class="well mod-center" {{ $page->main_color != '' ? "style='background: $page->main_color" : ''}}>
            <div class="container">
                <div class="row">
                    <div class="grid_12">
                        <h2 class="mg-add">
                            <span>
                                {!! $page['title_body'] !!}
                            </span>
                        </h2>
                        <h4 class="color-2">
                            {!! $page['slogan_body'] !!}
                        </h4>
                        {!! $page['body'] !!}
                    </div>
                </div>
            </div>
        </section>
        @if(count($news) > 0)
        <section class="well2 bg-content2 mod-center">
            <div class="container">

                <div class="mod-clear">
                    <div class="mod-position1">
                        <h3 class="color-2">{{ trans('labels.news.last') }}</h3>
                    </div>

                    <a class="btn btn-pull-right pulse mg-add color-2" href="{{ route('news') }}"><span>{!! trans('labels.news.view_all') !!}</span></a>
                </div>
                @foreach($news->chunk(3) as $three_news)
                    <div class="row">
                        @foreach($three_news as $key => $article)
                            <div class="grid_4 block1 wow {!! $wowClasses[$key] !!}" style="visibility: visible; animation-name: {!! $wowClasses[$key] !!};">
                                <img src="{!! asset('uploads/news/' . $article['image']) !!}" alt="{!! $article['name'] !!}">

                                <div class="block1_overlay">
                                    <div class="block1_city">
                                        <a href="{!! route('article', ['article' => $article->slug]) !!}">{!! $article['name'] !!}</a>
                                        {{--<div class="p__skin">{!! $article['description'] !!}</div>--}}
                                    </div>
                                    <a class="fa fa-search" href="{!! route('article', ['article' => $article->slug]) !!}"></a>
                                    <time datetime="{!! $article->date !!}">
                                        {!! preg_split("/[\s,]+/",$article['human date'])[0] !!}
                                        <br>
                                        <span>{!! preg_split("/[\s,]+/",$article['human date'])[1] !!}</span>
                                    </time>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </section>
        @endif

        @if (count($music) > 0)
            <section class="well well__ins">
                <div class="container">
                    <div class="mod-clear">
                        <div class="mod-position1">
                            <h3 class="mod-center">{{ trans('labels.music.musics') }}</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="fixed-player">
                            <audio id="player" preload="auto"></audio>
                        </div>
                        @foreach($music->chunk(3) as $i => $three_songs)
                            <div class="block2 grid_4 mg-add2">
                                <ul class="music-list" data-player-id="player">
                                    @foreach($three_songs as $song)
                                        <li data-src="{{ url('uploads/music/' . $song['file']) }}">
                                            <h4 class="h4__mod">
                                                {{ $song['name'] }}<br>
                                                <span class="color-2">{{ $song['category']['name'] }}</span>
                                            </h4>
                                        </li>
                                    @endforeach
                                </ul>
                                @if($i == count($music->chunk(3)) - 1)
                                    <a class="btn2 pulse" href="{{ url('music') }}"><span>Показать все</span></a>
                                @endif
                            </div>
                        @endforeach

                    </div>
                </div>
            </section>
        @endif

        @if($page['additional_body'] != '')
        <section class="well2 well2__ins parallax center" data-url="@if($page->additional_bg) {{ url('uploads/pages/' . $page->additional_bg) }} @else {{ url('images/parallax1.jpg') }} @endif" data-mobile="true" data-speed="0.6">
            <div class="container wow fadeInUp" data-wow-duration="2s">
                @if(isset( $page['title_additional_body'] ))
                    <h3 class="color-1">
                        {!! $page['title_additional_body'] !!}
                    </h3>
                @endif
                @if(isset( $page['slogan_additional_body'] ))
                    <h4 class="color-2 h4__mod">
                        {!! $page['slogan_additional_body'] !!}
                    </h4>
                @endif

                <p class="color-1 p-mod1">
                    {!! $page['additional_body'] !!}
                </p>
            </div>
        </section>
        @endif

        <section class="well3 bg-primary wow fadeInLeft animated" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInLeft;">
            <div class="container">
                <div class="box">
                    <form id="subscribe-form" class="subscribe-form">
                        <h4>Подписаться на новости</h4>
                        <label class="email">
                            <input type="email" value="Your email address">
                            <span class="error">*Invalid email.</span>
                            <span class="success" style="display: none;">Запрос на подиску отправлен!</span>
                        </label>
                        <a data-type="submit" href="#">Подписаться</a>
                    </form>
                </div>
            </div>
        </section>
    </main>
@endsection