<html>
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <title>Sign in</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link href="{{URL::asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('admin/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('admin/css/style.css')}}" rel="stylesheet">

    @yield('styles')

            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="page-container">
    <div class="content-container">
        <div class="login-container">
            <div class="content">
                <div class="panel panel-default login-form">
                    <div class="logo"><a href="{{ url('/login') }}">{{ env('PROJECT_NAME') }}</a></div>
                    <div class="panel-body">

                        @yield('content')

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
