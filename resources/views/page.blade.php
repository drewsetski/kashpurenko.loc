@extends('app')

@section('header')
    <header class="header__mod" @if($page->main_bg) style="background-image: url({{ url('uploads/pages/' . $page->main_bg) }})" @endif>
        @include('partials._nav')
    </header>
@endsection

@section('content')
    <main>

        <section class="well2 well2__ins" {{ $page->main_color != '' ? "style='background: $page->main_color" : ''}}>
            <div class="container">
                <div class="row">
                    <div class="grid_12 ">
                        <h2 class="mg-add center">
                            <span>
                                {!! $page['title_body'] !!}
                            </span>
                        </h2>
                        <h4 class="color-2 center">
                            {!! $page['slogan_body'] !!}
                        </h4>
                        {!! $page['body'] !!}
                    </div>
                </div>
            </div>
        </section>

        @if( $page['additional_body'] != '' )
            <section class="well2 well2__ins parallax center" data-url="images/parallax2.jpg" data-mobile="true" data-speed="0.6">
                <div class="container wow fadeInUp" data-wow-duration="2s">
                    @if(isset( $page['title_additional_body'] ))
                        <h3 class="color-1">
                            {!! $page['title_additional_body'] !!}
                        </h3>
                    @endif
                    @if(isset( $page['slogan_additional_body'] ))
                        <h4 class="color-2 h4__mod">
                            {!! $page['slogan_additional_body'] !!}
                        </h4>
                    @endif

                    <p class="color-1 p-mod1">
                        {!! $page['additional_body'] !!}
                    </p>
                </div>
            </section>
        @endif

    </main>
@endsection