@extends('app')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/gallery.css')}}">
@endsection

@section('header')
    <header class="header__mod" @if($page->main_bg) style="background-image: url({{ url('uploads/pages/' . $page->main_bg) }})" @endif>
        @include('partials._nav')
    </header>
@endsection

@section('content')
    <main>
        <section class="well2" {{ $page->main_color != '' ? "style='background: $page->main_color" : ''}}>
            <div class="container">
                <h3 class="mod-center">{{ trans('labels.gallery.categories') }}</h3>

                <div class="row">
                    @foreach($categories as $category)
                        <figure class="grid_4 effect-lily">
                            <img src="{{ url('uploads/categories/' . $category->image) }}" alt="img21"/>
                            <figcaption>
                                <h2>{{ $category->meta_title }}</h2>
                                <p>{{ $category->meta_description }}</p>
                                <a href="{{ url('gallery/' . $category->link) }}">View more</a>
                            </figcaption>
                        </figure>
                    @endforeach
                </div>

            </div>
        </section>
    </main>
@endsection
