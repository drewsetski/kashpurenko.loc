
<div id="stuck_container" class="stuck_container">
    <div class="container">
        <div class="brand">
            <h1 class="brand_name">
                <a href="{{ route('index') }}">
                    @if($settings['logo'] != '')
                        <img style="display: inline-block" src="{{ url('uploads/logo/' . $settings['logo']) }}" alt="{{ $settings['logo_title'] }}">
                    @endif
                    <div style="display: inline-block; vertical-align: bottom">
                        {!! preg_split("/[\s,]+/",$settings['logo_title'])[0] !!} <br>
                        {!!  preg_split("/[\s,]+/",$settings['logo_title'])[1] !!}
                    </div>
                </a>
            </h1>
            @if(isset($settings['logo_slogan']))
                <span class="brand_slogan">
                    {!! $settings['logo_slogan'] !!}
                </span>
            @endif
        </div>

        <nav class="nav">
            <ul class="sf-menu" data-type="navbar">
                @if(count($pages)>0)
                    @foreach($pages as $page)
                        <li @if(Route::currentRouteNamed($page->slug)) class="active" @endif >
                            <a href="{{URL::route($page->slug)}}">{!! $page->name !!}</a>
                        </li>
                    @endforeach
                @endif
                @if(count($other_pages)>0)
                    @foreach($other_pages as $page)
                        <li @if(Request::segment(1) == $page->slug) class="active" @endif >
                            <a href="{{url($page->slug)}}">{!! $page->name !!}</a>
                        </li>
                    @endforeach
                @endif
            </ul>
        </nav>
    </div>
</div>