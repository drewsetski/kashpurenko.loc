@extends('app')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/gallery.css')}}">
@endsection

@section('header')
    <header class="header__mod" @if($page->main_bg) style="background-image: url({{ url('uploads/pages/' . $page->main_bg) }})" @endif>
        @include('partials._nav')
    </header>
@endsection

@section('content')
    <main>
        <section class="well2" {{ $page->main_color != '' ? "style='background: $page->main_color" : ''}}>
            <div class="container">
                <h3 class="mod-center">{{ trans('labels.gallery.categories') }}</h3>

                <div class="row">
                    @foreach($categories as $category)
                        <div class="grid_4 block4" data-equal-group="1">
                            <a href="{{ url('music/' . $category->link) }}">
                                <div class="thumb"></div>
                                <h4 class="color-2 fw">{{ $category->name }}</h4>
                                <p>{{ $category->meta_description }}</p>
                            </a>
                        </div>
                    @endforeach
                </div>

            </div>
        </section>
    </main>
@endsection
