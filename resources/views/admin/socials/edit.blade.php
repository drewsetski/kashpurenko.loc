@extends('admin.app')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    {!! Form::model($social, [
                        'url' => url('master/socials/' . $social->id),
                        'class' => 'form',
                        'method' => 'PUT'
                    ]) !!}
                    {{-- Social name--}}
                    <div class="col-md-5 p0">
                        {!! Form::select('social', $socials, $social->key, ['placeholder' => 'Выберите сеть...', 'class' => 'form-control', 'required',]) !!}
                    </div>
                    {{--link--}}
                    <div class="col-md-5">
                        {!! Form::input("url", "link", $social->value,['class' => 'form-control']) !!}
                    </div>

                    {!! Form::submit('Submit', ['class' => 'btn btn-success col-md-2']) !!}
                    {!! Form::close() !!}
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection


