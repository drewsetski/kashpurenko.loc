@extends('admin.app')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    {!! Form::open([
                        'url' => url('master/socials'),
                        'class' => 'form',
                    ]) !!}
                    {{-- Social name--}}
                    <div class="col-md-5 p0">
                        {!! Form::select('social', $socials, null, ['placeholder' => 'Выберите сеть...', 'class' => 'form-control', 'required',]) !!}
                    </div>
                    {{--link--}}
                    <div class="col-md-5">
                        {!! Form::input("url", "link", null,['class' => 'form-control']) !!}
                    </div>

                    {!! Form::submit('Submit', ['class' => 'btn btn-success col-md-2']) !!}
                    {!! Form::close() !!}
                </div>
                <div class="col-md-12">
                    <table id="socials-table" data-toggle="table"
                           data-url="{{ url('master/socials/getSocials') }}"
                           data-search="true"
                           data-route="{{ url('master/socials') }}"
                           data-pagination="true"
                           data-locale="ru-RU"
                           data-sort-order="asc">
                        <thead>
                            <tr>
                                <th data-field="id" data-sortable="true" class="hidden">ID</th>
                                <th data-field="key" data-formatter="iconFormatter" data-sortable="true">Social</th>
                                <th data-field="value" data-formatter="linkFormatter" data-sortable="true">Link</th>
                                <th data-field="id" data-formatter="actions" data-width="100px">Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection


