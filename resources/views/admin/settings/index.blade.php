@extends('admin.app')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')

                    {!! Form::open(array( 'class' => 'form-horizontal', 'url' => route('admin.settings.update'), 'role' => 'form',  'files' => true  )) !!}
                        <!-- Email Field -->
                        <div class="form-group">
                            {!! Form::label('email', trans('labels.email'), array('class' => 'control-label  col-sm-2')) !!}
                            <div class="col-sm-10">
                                {!! Form::text('email', $settings->email, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <!--- Logo Field --->
                        <div class="form-group">
                            {!! Form::label('logo', trans('labels.logo'), ['class'=>'control-label col-sm-2'] ) !!}
                            <div class="col-sm-10">
                                {!! Form::file('logo', ['class' => 'filestyle form-control', 'data-value' => null, 'data-buttonName' => 'btn-brand', 'data-icon' => 'true' ] ) !!}
                            </div>
                        </div>
                        @if($settings->logo)
                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <img style="max-width:500px" src="/uploads/logo/{!! $settings->logo !!}">
                                </div>
                            </div>
                        @endif

                        <!-- Logo Title Field -->
                        <div class="form-group">
                            {!! Form::label('logo_title', trans('labels.logo_title'), array('class' => 'control-label  col-sm-2')) !!}
                            <div class="col-sm-10">
                                {!! Form::text('logo_title', $settings->logo_title, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <!-- Logo Slogan Field -->
                        <div class="form-group">
                            {!! Form::label('logo_slogan', trans('labels.logo_slogan'), array('class' => 'control-label  col-sm-2')) !!}
                            <div class="col-sm-10">
                                {!! Form::text('logo_slogan', $settings->logo_slogan, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <!-- Slogan H1 Field -->
                        <div class="form-group">
                            {!! Form::label('slogan_h1', trans('labels.slogan_h1'), array('class' => 'control-label  col-sm-2')) !!}
                            <div class="col-sm-10">
                                {!! Form::text('slogan_h1', $settings->slogan_h1, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <!-- Slogan H2 Field -->
                        <div class="form-group">
                            {!! Form::label('slogan_h2', trans('labels.slogan_h2'), array('class' => 'control-label  col-sm-2')) !!}
                            <div class="col-sm-10">
                                {!! Form::text('slogan_h2', $settings->slogan_h2, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <!-- Slogan Text Field -->
                        <div class="form-group">
                            {!! Form::label('slogan_text', trans('labels.slogan_text'), array('class' => 'control-label  col-sm-2')) !!}
                            <div class="col-sm-10">
                                {!! Form::textarea('slogan_text', $settings->slogan_text, array('class' => 'form-control')) !!}
                            </div>
                        </div>
                        {!! Form::button('<i class="fa fa-floppy-o fa-2x"></i>', array('type' => 'submit', 'class' => 'btn btn-success btn-flat')) !!}
                    {!! Form::close() !!}
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

@section('scripts')

@endsection