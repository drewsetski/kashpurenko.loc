@extends('admin.app')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    @if($category === null)
                        {!! Form::model($category, ['url' => url('master/music/categories') , 'method' => 'POST', 'files' => true]) !!}
                    @else
                        {!! Form::model($category, ['url' => url('master/music/categories/edit/' . $category->id) , 'method' => 'PUT', 'files' => true]) !!}
                    @endif
                        <!--- Name Field --->
                        <div class="form-group">
                            {!! Form::label('name', trans('labels.name'), ['class'=>'control-label']) !!}
                            {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
                        </div>

                        <!--- Name Field --->
                        <div class="form-group">
                            {!! Form::label('link', trans('labels.link'), ['class'=>'control-label']) !!}
                            {!! Form::text('link', null, ['class' => 'form-control', 'required']) !!}
                        </div>

                        <!--- Meta Title Field --->
                        <div class="form-group">
                            {!! Form::label('meta_title', trans('labels.title'), ['class'=>'control-label']) !!}
                            {!! Form::text('meta_title', null, ['class' => 'form-control', 'required']) !!}
                        </div>

                        <!--- Meta Keywords Field --->
                        <div class="form-group">
                            <label for="meta_keywords">{{ trans('labels.keywords') }}</label>
                            <select name="meta_keywords[]" id="meta_keywords" class="keywords" multiple data-placeholder="Слова">
                                @if(isset($category->meta_keywords))
                                    @foreach($category->meta_keywords as $keyword)
                                        <option value="{{$keyword}}" selected>{{$keyword}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <!--- Meta Description Field --->
                        <div class="form-group">
                            {!! Form::label('meta_description', trans('labels.description'), ['class'=>'control-label']) !!}
                            {!! Form::textarea('meta_description', null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12 ">
                                <button type="submit" id="inst_submit" class="btn btn-success btn-flat"><i class="fa fa-floppy-o fa-2x"></i></button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

@section('scripts')
    <script src="{{URL::asset('admin/js/chosen.proto.min.js')}}"></script>
    <script>
        meta_title_touched = false;
        url_touched = false;

        $('input[name="link"]').change(function() { url_touched = true; });

        $('input[name="name"]').keyup(function() {
            if(!url_touched)
                $('input[name="link"]').val(generate_url());

            if(!meta_title_touched)
                $('input[name="meta_title"]').val( $('input[name="name"]').val() );
        });

        $('input[name="meta_title"]').change(function() { meta_title_touched = true; });

        function generate_url()
        {
            url = $('input[name="name"]').val();
            url = url.replace(/[\s]+/gi, '-');
            url = translit(url);
            url = url.replace(/[^0-9a-z_\-]+/gi, '').toLowerCase();
            return url;
        }

        function translit(str)
        {
            var ru=("А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ъ-ъ-Ы-ы-Ь-ь-Э-э-Ю-ю-Я-я").split("-")
            var en=("A-a-B-b-V-v-G-g-G-g-D-d-E-e-E-e-E-e-ZH-zh-Z-z-I-i-I-i-I-i-J-j-K-k-L-l-M-m-N-n-O-o-P-p-R-r-S-s-T-t-U-u-F-f-H-h-TS-ts-CH-ch-SH-sh-SCH-sch-'-'-Y-y-'-'-E-e-YU-yu-YA-ya").split("-")
            var res = '';
            for(var i=0, l=str.length; i<l; i++)
            {
                var s = str.charAt(i), n = ru.indexOf(s);
                if(n >= 0) { res += en[n]; }
                else { res += s; }
            }
            return res;
        }
    </script>
@endsection

