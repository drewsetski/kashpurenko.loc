@extends('admin.app')

@section('content')
    <main>
        <a href="{{ url('master/music/musics/' . $category->id . '/add') }}" class="btn btn-brand btn-flat"><i class="fa fa-plus fa-2x"></i></a>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    @if (count($musics) > 0)
                        {!! Form::open(['url' => url('master/music/musics/' . $category->id), 'method' => 'DELETE', 'id'=>'list' ]) !!}
                        <div class="row">
                            @foreach ($musics as $music)
                                <div class="col-md-3">
                                    <div class="thumbnail text-center">
                                        <audio controls style="width: 100%;">
                                            <source src="{{ url('uploads/music/' . $music->file) }}">
                                        </audio>
                                        <div class="caption">
                                            <h4 class="mt5 mb5" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;">{!! $music->name !!}</h4>
                                            <div class="btn-group photo-btn-group">
                                                <div class="checkbox checkbox-brand inline pull-left"><input style="margin: 0;" name="check[]" value="{{ $music->id }}" id="{!! $music->id !!}" type="checkbox"><label
                                                            for="{!! $music->id !!}"></label></div>
                                                <a href="{{ url('master/music/musics/' . $category->id . '/' . $music->id) }}" class="btn btn-info"><span class="glyphicon glyphicon-pencil" style="line-height: 22px;"></span></a>
                                                <button title="Удалить" type="button" class="delete-photo btn btn-danger" data-id="{{ $music->id }}"><span class="glyphicon glyphicon-remove"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="select_form">
                            <label id="check_all" class="btn btn-primary">{{ trans('labels.select_all') }}</label>
                            <input type="hidden" name="action" value="delete">
                            <button type="submit" style="margin-left: 20px;" class="btn btn-success">{{ trans('labels.delete') }}</button>
                        </div>
                        <div class="form-group"></div>

                        {!! Form::close() !!}
                    @else
                        <div class="alert alert-warning empty" role="alert">
                            Нет песен
                        </div>
                    @endif
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

@section('scripts')
    <script>
        $(function() {
            // Удаление записи
            $('.delete-photo').click( function() {
                var this_check = $(this).closest(".btn-group").find('input[type="checkbox"][name*="check"]');
                $('input[type="checkbox"][name*="check"]').not(this_check).attr('checked', false);
                $(this).closest(".btn-group").find('input[type="checkbox"][name*="check"]').attr('checked', true);
                $(this).closest("form#list").submit();
            });

            // Удаление записей
            $("form#list").submit(function(e) {
                var currentForm = this;
                e.preventDefault();
                if ($("form#list input:checkbox:checked").length > 0){
                    bootbox.confirm("Вы уверены, что хотите удалить выбранную запись?", function (result) {
                        if (result) {
                            currentForm.submit();
                        }
                    });
                }
            });

            // Выделить все
            $("#check_all").on( 'click', function() {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length>0 );
            });
        })
    </script>
@endsection
