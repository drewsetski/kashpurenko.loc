<div class="row table-responsive" style="width: auto;">
    <table id="pages-table"
           data-route="{{ route('admin.pages.index') }}"
           data-url="{{ route('admin.pages.get') }}"
           data-toggle="table"
           data-page-size="10"
           data-search="true"
           data-locale="ru-RU"
           data-side-pagination="server"
           data-pagination="true"
           data-row-style="rowStyle"
           data-query-params="queryParams">
        <thead>
        <tr>
            <th data-field="id" data-formatter="ids" class="hidden">Id</th>
            <th data-field="name">{{ trans('labels.name') }}</th>
            <th data-field="slug">{{ trans('labels.slug') }}</th>
            <th data-field="id" data-formatter="actions" data-width="100px">{{ trans('labels.actions') }}</th>
        </tr>
        </thead>
    </table>
</div>