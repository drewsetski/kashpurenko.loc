@extends('admin.app')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    @if($pages_count > 0)
                            @include('admin.pages.partials._pages_table')
                        @else
                        <div class="alert empty warning">
                            {{ trans('labels.pages.empty') }}
                        </div>
                    @endif
                    <a href="{!! route('admin.pages.add') !!}" title="{{ trans('labels.pages.add') }}" class="btn btn-brand btn-flat"><i class="fa fa-plus fa-2x"></i></a>
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

@section('scripts')

@endsection