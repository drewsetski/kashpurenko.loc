@extends('admin.app')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    {!! Form::model($page, array('url' => $route, 'class' => 'form-horizontal', 'method' => $method, 'files' => true)) !!}
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#record" aria-controls="record" role="tab" data-toggle="tab">{{ trans('labels.pages.record') }}</a></li>
                            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">{{ trans('labels.pages.settings') }}</a></li>
                        </ul>
                        <div class="tab-content">

                            <!-- Tab for record -->
                            <div class="tab-pane active" id="record">
                                <div class="form-group">&nbsp;</div>

                                <!-- Name field -->
                                <div class="form-group">
                                    {!! Form::label('name', trans('labels.name'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('name', null, array('class' => 'form-control')) !!}
                                    </div>
                                </div>

                                <!-- Title field -->
                                <div class="form-group">
                                    {!! Form::label('title_body', trans('labels.title_body'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('title_body', null, array('class' => 'form-control')) !!}
                                    </div>
                                </div>

                                <!-- Slogan field -->
                                <div class="form-group">
                                    {!! Form::label('slogan_body', trans('labels.slogan_body'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('slogan_body', null, array('class' => 'form-control')) !!}
                                    </div>
                                </div>

                                <!-- Body field -->
                                <div class="form-group">
                                    {!! Form::label('body', trans('labels.body'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        {!! Form::textarea('body',  null, array('class'=>'form-control editor') ) !!}
                                    </div>
                                </div>

                                <!--- Main Background Field --->
                                <div class="form-group">
                                    {!! Form::label('main_bg', trans('labels.main_bg'), ['class'=>'control-label col-sm-2'] ) !!}
                                    <div class="col-sm-10">
                                        {!! Form::file('main_bg', ['class' => 'filestyle form-control', 'data-value' => null, 'data-buttonName' => 'btn-brand', 'data-icon' => 'true' ] ) !!}
                                    </div>
                                </div>
                                @if($page->main_bg)
                                    <div class="form-group">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10">
                                            <img style="max-width:500px" src="/uploads/pages/{!! $page->main_bg !!}">
                                        </div>
                                    </div>
                                @endif
                                <!--- Main Background Field --->
                                <div class="form-group">
                                    {!! Form::label('main_color', trans('labels.main_color'), ['class'=>'control-label col-sm-2'] ) !!}
                                    <div class="col-sm-3">
                                        <div id="color-picker" class="input-group colorpicker-component">
                                            <input type="text" name="main_color" value="#FFFFFF" class="form-control" />
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                </div>

                                <!-- Title for an additional body on Paralax background field -->
                                <div class="form-group">
                                    {!! Form::label('title_additional_body', trans('labels.title_additional_body'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('title_additional_body', null, array('class' => 'form-control')) !!}
                                    </div>
                                </div>

                                <!-- Slogan for an additional body on Paralax background field -->
                                <div class="form-group">
                                    {!! Form::label('slogan_additional_body', trans('labels.slogan_additional_body'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('slogan_additional_body', null, array('class' => 'form-control')) !!}
                                    </div>
                                </div>

                                <!-- Additional Body on Paralax background field -->
                                <div class="form-group">
                                    {!! Form::label('additional_body', trans('labels.additional_body'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        {!! Form::textarea('additional_body',  null, array('class'=>'form-control editor') ) !!}
                                    </div>
                                </div>

                                <!--- Main Background Field --->
                                <div class="form-group">
                                    {!! Form::label('additional_bg', trans('labels.additional_bg'), ['class'=>'control-label col-sm-2'] ) !!}
                                    <div class="col-sm-10">
                                        {!! Form::file('additional_bg', ['class' => 'filestyle form-control', 'data-value' => null, 'data-buttonName' => 'btn-brand', 'data-icon' => 'true' ] ) !!}
                                    </div>
                                </div>
                                    @if($page->additional_bg)
                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10">
                                                <img style="max-width:500px" src="/uploads/pages/{!! $page->additional_bg !!}">
                                            </div>
                                        </div>
                                    @endif
                            </div>

                            <!-- Tab for settings -->
                            <div class="tab-pane" id="settings">
                                <div class="form-group">&nbsp;</div>
                                @if(!in_array($page->id, [1,2,3,4,5,6,7]))
                                    <!-- Slug field -->
                                    <div class="form-group">
                                        {!! Form::label('slug', trans('labels.slug'), array('class' => 'control-label  col-sm-2')) !!}
                                        <div class="col-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon">&nbsp;/&nbsp;</span>
                                                {!! Form::text('slug', null, array('class' => 'form-control')) !!}
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <!-- Meta title field -->
                                <div class="form-group">
                                    {!! Form::label('meta_title', trans('labels.title'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('meta_title', null, array('class' => 'form-control')) !!}
                                    </div>
                                </div>

                                <!-- Meta keywords field -->
                                <div class="form-group">
                                    {!! Form::label('meta_keywords', trans('labels.keywords'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        <select name="meta_keywords[]" id="meta_keywords" class="keywords" multiple data-placeholder="{{ trans('labels.words') }}">
                                            @if(isset($page->meta_keywords))
                                                @foreach($page->meta_keywords as $keyword)
                                                    <option value="{{$keyword}}" selected>{{$keyword}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <!-- Meta description field -->
                                <div class="form-group">
                                    {!! Form::label('meta_description', trans('labels.description'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        {!! Form::textarea('meta_description', null, array('class' => 'form-control')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::button('<i class="fa fa-floppy-o fa-2x"></i>', array('type' => 'submit', 'class' => 'btn btn-success btn-flat')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

@include('admin.tinymce_init')

@section('styles')
    <link href="{{URL::asset('admin/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet">
@endsection

@section('scripts')
    <script src="{{URL::asset('admin/js/bootstrap-colorpicker.min.js')}}"></script>
    <script>
        $(function(){
            $('#color-picker').colorpicker();
        });
        meta_title_touched = false;
        url_touched = false;

        $('input[name="slug"]').change(function() { url_touched = true; });

        $('input[name="name"]').keyup(function() {
            if(!url_touched)
                $('input[name="slug"]').val(generate_url());

            if(!meta_title_touched)
                $('input[name="meta_title"]').val( $('input[name="name"]').val() );
        });

        $('input[name="meta_title"]').change(function() { meta_title_touched = true; });

        function generate_url()
        {
            url = $('input[name="name"]').val();
            url = url.replace(/[\s]+/gi, '-');
            url = translit(url);
            url = url.replace(/[^0-9a-z_\-]+/gi, '').toLowerCase();
            return url;
        }

        function translit(str)
        {
            var ru=("А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ъ-ъ-Ы-ы-Ь-ь-Э-э-Ю-ю-Я-я").split("-")
            var en=("A-a-B-b-V-v-G-g-G-g-D-d-E-e-E-e-E-e-ZH-zh-Z-z-I-i-I-i-I-i-J-j-K-k-L-l-M-m-N-n-O-o-P-p-R-r-S-s-T-t-U-u-F-f-H-h-TS-ts-CH-ch-SH-sh-SCH-sch-'-'-Y-y-'-'-E-e-YU-yu-YA-ya").split("-")
            var res = '';
            for(var i=0, l=str.length; i<l; i++)
            {
                var s = str.charAt(i), n = ru.indexOf(s);
                if(n >= 0) { res += en[n]; }
                else { res += s; }
            }
            return res;
        }
    </script>
@endsection