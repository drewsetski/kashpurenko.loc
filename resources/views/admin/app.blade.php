<html>
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    @if ( isset( $title ) ) <title>{!! $title !!}</title>@endif
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link href="{{URL::asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('admin/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('admin/css/jquery.navobile.css')}}" rel="stylesheet">
    <link href="{{URL::asset('admin/css/animate.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('admin/css/check.css')}}" rel="stylesheet">
    <link href="{{URL::asset('admin/css/switchery.min.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('admin/css/chosen.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('admin/css/jquery.jgrowl.min.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('admin/css/bootstrap-table.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('admin/css/style.css')}}" rel="stylesheet">

    @yield('styles')

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="wrapper" id="content">
    <header class="header">
        <nav class="header-nav">
            <ul class="f-left">
                <li class="f-left">
                    <a href="#" id="show-sidebar" class="animated slideInLeftSmall"><i class="fa fa-bars fa-2x"></i></a>
                </li>
            </ul>
            <ul class="mainnav f-right">
                <li><a href="{{ url('/logout') }}" data-toggle="tooltip" data-placement="bottom" title="Log out"><i class="fa fa-sign-out fa-2x"></i></a></li>
            </ul>
        </nav>
    </header><!-- .header-->

    <nav class="left-sidebar">
        <div class="sidebar-container">
            <div class="logo"><a href="/">{!! env('PROJECT_NAME') !!}</a></div>
            <ul>
                <li {{ Request::is('/') ? ' class=active' : null }}><a href="{{ route('admin.index') }}"><i class="fa fa-home fa-2x"></i><span>{{ trans('labels.home') }}</span></a></li>
                <li {{ Request::segment(2) == 'settings' ? ' class=active' : null }}><a href="{{ route('admin.settings.index') }}"><i class="fa fa-cogs fa-2x"></i><span>{{ trans('labels.settings') }}</span></a></li>
                <li {{ Request::segment(2) == 'contacts' ? ' class=active' : null }}><a href="{{ route('admin.contacts.index') }}"><i class="fa fa-phone-square fa-2x"></i><span>{{ trans('labels.contacts.contacts') }}</span></a></li>
                <li {{ Request::segment(2) == 'pages' ? ' class=active' : null }}><a href="{{ route('admin.pages.index') }}"><i class="fa fa-globe fa-2x"></i><span>{{ trans('labels.pages.pages') }}</span></a></li>
                <li {{ Request::segment(2) == 'news' ? ' class=active' : null }}><a href="{{ route('admin.news.index') }}"><i class="fa fa-newspaper-o fa-2x"></i><span>{{ trans('labels.news.news') }}</span></a></li>
                <li {{ Request::segment(2) == 'slides' ? ' class=active' : null }}><a href="{{ route('admin.slides.index') }}"><i class="fa fa-picture-o fa-2x"></i><span>{{ trans('labels.slides.slides') }}</span></a></li>
                <li>
                    <a href="{{ url('master/gallery') }}" class="has-dropdown"><i class="fa fa-camera-retro fa-2x"></i><span>{{ trans('labels.gallery.gallery') }}</span></a>
                    <ul class="hidden-dropdown">
                        <li {{ (Request::segment(2) == 'gallery' && Request::segment(3) == 'categories') ? ' class=active' : null }}><a href="{{ url('master/gallery/categories') }}"><i class="fa fa-font fa-2x"></i>{{ trans('labels.gallery.categories') }}</a></li>
                        <li {{ Request::segment(3) == 'photos' ? ' class=active' : null }}><a href="{{ url('master/gallery/photos') }}"><i class="fa fa-camera fa-2x"></i>{{ trans('labels.gallery.photos') }}</a></li>
                    </ul>
                </li>
                <li {{ Request::segment(2) == 'videos' ? ' class=active' : null }}><a href="{{ route('admin.videos.index') }}"><i class="fa fa-video-camera fa-2x"></i><span>{{ trans('labels.videos.videos') }}</span></a></li>
                <li>
                    <a href="{{ url('master/music') }}" class="has-dropdown"><i class="fa fa-music fa-2x"></i><span>{{ trans('labels.music.music') }}</span></a>
                    <ul class="hidden-dropdown">
                        <li {{ (Request::segment(2) == 'music' && Request::segment(3) == 'categories') ? ' class=active' : null }}><a href="{{ url('master/music/categories') }}"><i class="fa fa-font fa-2x"></i>{{ trans('labels.gallery.categories') }}</a></li>
                        <li {{ Request::segment(3) == 'musics' ? ' class=active' : null }}><a href="{{ url('master/music/musics') }}"><i class="fa fa-play fa-2x"></i>{{ trans('labels.music.musics') }}</a></li>
                    </ul>
                </li>
                <li {{ Request::segment(2) == 'socials' ? ' class=active' : null }}><a href="{{ url('master/socials') }}"><i class="fa fa-share-square fa-2x"></i><span>{{ trans('labels.socials') }}</span></a></li>
            </ul>
        </div>

        @include('partials._footer')
    </nav>

    @yield('content')

</div><!-- end of wrapper -->

    <!-- Scripts -->
    <script src="{{URL::asset('admin/js/jquery.js')}}"></script>
    <script src="{{URL::asset('admin/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('admin/js/bootstrap-filestyle.min.js')}}"></script>
    <script src="{{URL::asset('admin/js/bootbox.js')}}"></script>
    <script src="{{URL::asset('admin/js/jquery.navobile.min.js')}}"></script>
    <script src="{{URL::asset('admin/js/switchery.min.js')}}"></script>
    <script src="{{URL::asset('admin/js/chosen.jquery.min.js')}}"></script>
    <script src="{{URL::asset('admin/js/jquery.jgrowl.min.js')}}"></script>
    <script src="{{URL::asset('admin/js/bootstrap-table.min.js')}}"></script>
    <script src="{{URL::asset('admin/js/bootstrap-table-locale-all.min.js')}}"></script>
    <script src="{{URL::asset('admin/js/app.js')}}"></script>

    @yield('scripts')

</body>
</html>