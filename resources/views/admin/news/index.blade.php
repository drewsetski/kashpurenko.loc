@extends('admin.app')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    @if($news_count > 0)
                            @include('admin.news.partials._news_table')
                        @else
                        <div class="alert empty warning">
                            {{ trans('labels.news.empty') }}
                        </div>
                    @endif
                    <a href="{!! route('admin.news.add') !!}" title="{{ trans('labels.news.add') }}" class="btn btn-brand btn-flat"><i class="fa fa-plus fa-2x"></i></a>
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

@section('scripts')

@endsection