@extends('admin.app')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')

                    {!! Form::model($article, array('url' => $route, 'class' => 'form-horizontal', 'method' => $method, 'files' => true)) !!}
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#record" aria-controls="record" role="tab" data-toggle="tab">{{ trans('labels.news.record') }}</a></li>
                            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">{{ trans('labels.news.settings') }}</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="record">
                                <div class="form-group">&nbsp;</div>

                                <!-- Name Field -->
                                <div class="form-group">
                                    {!! Form::label('name', trans('labels.name'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('name', null, array('class' => 'form-control')) !!}
                                    </div>
                                </div>

                                <!-- Image Field --->
                                <div class="form-group">
                                    {!! Form::label('image', trans('labels.image'), ['class'=>'control-label col-sm-2'] ) !!}
                                    <div class="col-sm-10">
                                        {!! Form::file('image', ['class' => 'filestyle form-control', 'data-value' => null, 'data-buttonName' => 'btn-brand', 'data-icon' => 'true' ] ) !!}
                                    </div>
                                </div>

                                @if(!empty($article->image))
                                    <div class="form-group">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10">
                                            <img src="{{ asset('uploads/news/' . $article->image) }}" alt="{{ $article->name }}">
                                        </div>
                                    </div>
                                @endif

                                <!-- Description Field -->
                                <div class="form-group">
                                    {!! Form::label('description', trans('labels.description'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        {!! Form::textarea('description',  null, array('class'=>'form-control editor') ) !!}
                                    </div>
                                </div>

                                <!-- Body Field -->
                                <div class="form-group">
                                    {!! Form::label('body', trans('labels.body'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        {!! Form::textarea('body',  null, array('class'=>'form-control editor') ) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="settings">
                                <div class="form-group">&nbsp;</div>

                                <!-- Slug Field -->
                                <div class="form-group">
                                    {!! Form::label('slug', trans('labels.slug'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">&nbsp;/news/&nbsp;</span>
                                                {!! Form::text('slug', null, array('class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Meta title field -->
                                <div class="form-group">
                                    {!! Form::label('meta_title', trans('labels.title'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('meta_title', null, array('class' => 'form-control')) !!}
                                    </div>
                                </div>

                                <!-- Meta keywords field -->
                                <div class="form-group">
                                    {!! Form::label('meta_keywords', trans('labels.keywords'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        <select name="meta_keywords[]" id="meta_keywords" class="keywords" multiple data-placeholder="{{ trans('labels.words') }}">
                                            @if(isset($article->meta_keywords))
                                                @foreach($article->meta_keywords as $keyword)
                                                    <option value="{{$keyword}}" selected>{{$keyword}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <!-- Meta description field -->
                                <div class="form-group">
                                    {!! Form::label('meta_description', trans('labels.description'), array('class' => 'control-label  col-sm-2')) !!}
                                    <div class="col-sm-10">
                                        {!! Form::textarea('meta_description', null, array('class' => 'form-control')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::button('<i class="fa fa-floppy-o fa-2x"></i>', array('type' => 'submit', 'class' => 'btn btn-success btn-flat')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

@include('admin.tinymce_init')

@section('scripts')
    <script>
        meta_title_touched = false;
        url_touched = false;

        $('input[name="slug"]').change(function() { url_touched = true; });

        $('input[name="name"]').keyup(function() {
            if(!url_touched)
                $('input[name="slug"]').val(generate_url());

            if(!meta_title_touched)
                $('input[name="meta_title"]').val( $('input[name="name"]').val() );
        });

        $('input[name="meta_title"]').change(function() { meta_title_touched = true; });

        function generate_url()
        {
            url = $('input[name="name"]').val();
            url = url.replace(/[\s]+/gi, '-');
            url = translit(url);
            url = url.replace(/[^0-9a-z_\-]+/gi, '').toLowerCase();
            return url;
        }

        function translit(str)
        {
            var ru=("А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ъ-ъ-Ы-ы-Ь-ь-Э-э-Ю-ю-Я-я").split("-")
            var en=("A-a-B-b-V-v-G-g-G-g-D-d-E-e-E-e-E-e-ZH-zh-Z-z-I-i-I-i-I-i-J-j-K-k-L-l-M-m-N-n-O-o-P-p-R-r-S-s-T-t-U-u-F-f-H-h-TS-ts-CH-ch-SH-sh-SCH-sch-'-'-Y-y-'-'-E-e-YU-yu-YA-ya").split("-")
            var res = '';
            for(var i=0, l=str.length; i<l; i++)
            {
                var s = str.charAt(i), n = ru.indexOf(s);
                if(n >= 0) { res += en[n]; }
                else { res += s; }
            }
            return res;
        }
    </script>
@endsection