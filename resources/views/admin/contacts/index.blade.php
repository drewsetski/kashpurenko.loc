@extends('admin.app')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    @if($contacts_count > 0)
                            @include('admin.contacts.partials._contacts_table')
                        @else
                        <div class="alert empty warning">
                            {{ trans('labels.contacts.empty') }}
                        </div>
                    @endif
                    <a href="{!! route('admin.contacts.add') !!}" title="{{ trans('labels.contacts.add') }}" class="btn btn-brand btn-flat"><i class="fa fa-plus fa-2x"></i></a>
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

@section('scripts')

@endsection