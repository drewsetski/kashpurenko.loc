@extends('admin.app')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    {!! Form::model($contact, array('url' => $route, 'class' => 'form-horizontal', 'method' => $method)) !!}
                        <!-- Name field -->
                        <div class="form-group">
                            {!! Form::label('name', trans('labels.name'), array('class' => 'control-label  col-sm-2')) !!}
                            <div class="col-sm-10">
                                {!! Form::text('name', null, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <!-- Email field -->
                        <div class="form-group">
                            {!! Form::label('phone', trans('labels.phone'), array('class' => 'control-label  col-sm-2')) !!}
                            <div class="col-sm-10">
                                {!! Form::text('phone',  null, array('class'=>'form-control') ) !!}
                            </div>
                        </div>

                        <!-- Email field -->
                        <div class="form-group">
                            {!! Form::label('email', trans('labels.email'), array('class' => 'control-label  col-sm-2')) !!}
                            <div class="col-sm-10">
                                {!! Form::text('email',  null, array('class'=>'form-control') ) !!}
                            </div>
                        </div>

                        <!-- Description field -->
                        <div class="form-group">
                            {!! Form::label('description', trans('labels.description'), array('class' => 'control-label  col-sm-2')) !!}
                            <div class="col-sm-10">
                                {!! Form::textarea('description',  null, array('class'=>'form-control editor') ) !!}
                            </div>
                        </div>
                        {!! Form::button('<i class="fa fa-floppy-o fa-2x"></i>', array('type' => 'submit', 'class' => 'btn btn-success btn-flat')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

@include('admin.tinymce_init')

@section('scripts')

@endsection