<div class="row table-responsive" style="width: auto;">
    <table id="pages-table"
           data-route="{{ route('admin.contacts.index') }}"
           data-url="{{ route('admin.contacts.get') }}"
           data-toggle="table"
           data-page-size="10"
           data-search="true"
           data-locale="ru-RU"
           data-side-pagination="server"
           data-pagination="true"
           data-row-style="rowStyle"
           data-query-params="queryParams">
        <thead>
        <tr>
            <th data-field="id" data-formatter="ids" class="hidden">Id</th>
            <th data-field="name">{{ trans('labels.name') }}</th>
            <th data-field="phone">{{ trans('labels.phone') }}</th>
            <th data-field="email">{{ trans('labels.email') }}</th>
            <th data-field="id" data-formatter="actions" data-width="100px">{{ trans('labels.actions') }}</th>
        </tr>
        </thead>
    </table>
</div>