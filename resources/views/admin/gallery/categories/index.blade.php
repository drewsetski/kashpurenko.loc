@extends('admin.app')

@section('styles')
    <style>

    </style>
@endsection

@section('content')
    <main>
        <a href="{{ url('master/gallery/categories/add') }}" class="btn btn-brand btn-flat"><i class="fa fa-plus fa-2x"></i></a>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    @if( count($categories)>0 )

                        <div class="row">
                            @foreach ( $categories as $category )
                                <div class="col-md-3">
                                    <div class="thumbnail text-center">
                                        <a href="{{ url('master/gallery/photos/' . $category->id) }}">
                                            @if($category->image)
                                                <img style="max-height: 200px; display: block;" alt="icon" class="cat-icon" src="{{ asset('uploads/categories/' . $category->image) }}">
                                            @else
                                                <img style="max-height: 200px; display: block;" alt="icon" class="cat-icon" src="{{ asset('uploads/categories/default.png') }}">
                                            @endif
                                        </a>
                                        <div class="caption">
                                            <a href="{{ url('master/gallery/photos/' . $category->id) }}"><h4 class="mt5 mb5">{!! $category->name !!}</h4></a>
                                            <div class="btn-group photo-btn-group">

                                                <a href="{{ url('master/gallery/categories/edit/' . $category->id) }}" class="btn btn-info"><span class="glyphicon glyphicon-pencil" style="line-height: 22px;"></span></a>
                                                <a href="{{ url('master/gallery/photos/' . $category->id) }}" class="btn btn-primary"><span class="glyphicon glyphicon-share-alt" style="line-height: 22px;"></span></a>
                                                <button title="Удалить" type="button" class="delete-category btn btn-danger" data-route="/master/gallery/categories" data-id="{{ $category->id }}"><span class="glyphicon glyphicon-remove"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="alert alert-warning empty" role="alert">
                            Нет категорий
                        </div>
                    @endif
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

@section('scripts')
    <script>
        /**
         * Deleting record from table
         */
        $('body').on('click', '.delete-category', function(){
            var id = $(this).attr('data-id');
            var route = $(this).attr('data-route');
            var thumb = $(this).closest('.col-md-3')
            bootbox.confirm("Вы уверены, что хотите удалить запись?", function(result) {
                if(result) {
                    $.ajax({
                        url: route+ '/' + id,
                        type: 'DELETE',
                        success: function(){
                            thumb.remove();
                        },
                        error: function(error){
                            $.jGrowl( "Невозможно удалить запись", {
                                sticky:false,
                                theme: "danger",
                                header: "Ошибка"
                            });
                        }
                    });
                }
            });
        });
    </script>
@endsection

