@extends('admin.app')

@section('styles')
    <link href="{{URL::asset('admin/css/check.css')}}" rel="stylesheet">
@endsection

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                @include('flash::message')
                @if($photo === null)
                    {!! Form::model($photo, ['url' => url('master/gallery/photos/' . $category->id) , 'method' => 'POST', 'files' => true]) !!}
                @else
                    {!! Form::model($photo, ['url' => url('master/gallery/photos/' . $category->id . '/' . $photo->id) , 'method' => 'PUT', 'files' => true]) !!}
                @endif
                    @if($photo !== null)
                        <!--- Name Field --->
                        <div class="form-group">
                            {!! Form::label('name', trans('labels.name'), ['class'=>'control-label']) !!}
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        </div>

                        <!--- Name Field --->
                        <div class="form-group">
                            {!! Form::label('description', trans('labels.description'), ['class'=>'control-label']) !!}
                            {!! Form::text('description', null, ['class' => 'form-control']) !!}
                        </div>
                    @endif

                    <!--- Image Field --->
                    <div class="form-group">
                        {!! Form::label('image', trans('labels.image'), ['class'=>'control-label'] ) !!}
                        {!! Form::file('image[]', [
                            'class' => 'filestyle form-control',
                            'data-value' => null,
                            'data-buttonName' => 'btn-brand',
                            'data-icon' => 'true',
                            'multiple'
                         ]) !!}
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12 ">
                            <button type="submit" id="inst_submit" class="btn btn-success btn-flat"><i class="fa fa-floppy-o fa-2x"></i></button>
                        </div>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection


