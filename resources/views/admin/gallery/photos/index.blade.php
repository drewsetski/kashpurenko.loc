@extends('admin.app')

@section('styles')
    <style>

    </style>
@endsection

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    @if( count($categories)>0 )

                        <div class="row">
                            <h2 class="col-md-12">Выберите категорию</h2>
                            @foreach ( $categories as $category )
                                <div class="col-md-3">
                                    <div class="thumbnail text-center">
                                        <a href="{{ url('master/gallery/photos/' . $category->id) }}">
                                            @if($category->image)
                                                <img style="max-height: 200px; display: block;" alt="icon" class="cat-icon" src="{{ asset('uploads/categories/' . $category->image) }}">
                                            @else
                                                <img style="max-height: 200px; display: block;" alt="icon" class="cat-icon" src="{{ asset('uploads/categories/default.png') }}">
                                            @endif
                                        </a>
                                        <div class="caption">
                                            <a href="{{ url('master/gallery/photos/' . $category->id) }}"><h4 class="mt5 mb5">{!! $category->name !!}</h4></a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="alert alert-warning empty" role="alert">
                            Нет категорий
                        </div>
                    @endif
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

