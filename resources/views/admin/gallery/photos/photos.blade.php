@extends('admin.app')

@section('styles')
    <style>

    </style>
@endsection

@section('content')
    <main>
        <a href="{{ url('master/gallery/photos/' . $category->id . '/add') }}" class="btn btn-brand btn-flat"><i class="fa fa-plus fa-2x"></i></a>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    @if(count($photos)>0)
                        {!! Form::open(['url' => url('master/gallery/photos/' . $category->id), 'method' => 'DELETE', 'id'=>'list' ]) !!}
                        <div class="row">

                            @foreach ( $photos as $photo )
                                <div class="col-md-3">
                                    <div class="thumbnail text-center">
                                        <a href="{{ url('master/gallery/photos/' . $category->id . '/' . $photo->id) }}">
                                            @if($photo->file)
                                                <img style="max-height: 200px; display: block;" alt="icon" class="cat-icon" src="{{ asset('uploads/photos/' . $photo->file) }}">
                                            @else
                                                <img style="max-height: 200px; display: block;" alt="icon" class="cat-icon" src="{{ asset('uploads/categories/default.png') }}">
                                            @endif
                                        </a>
                                        <div class="caption">
                                            <a href="{{ url('master/gallery/photos/' . $category->id . '/' . $photo->id) }}"><h4 class="mt5 mb5">{!! $photo->name !!}</h4></a>
                                            <div class="btn-group photo-btn-group">
                                                <div class="checkbox checkbox-brand inline pull-left"><input style="margin: 0;" name="check[]" value="{{ $photo->id }}" id="{!! $photo->id !!}" type="checkbox"><label
                                                            for="{!! $photo->id !!}"></label></div>
                                                <a href="{{ url('master/gallery/photos/' . $category->id . '/' . $photo->id) }}" class="btn btn-info"><span class="glyphicon glyphicon-pencil" style="line-height: 22px;"></span></a>
                                                <button title="Удалить" type="button" class="delete-photo btn btn-danger" data-id="{{ $photo->id }}"><span class="glyphicon glyphicon-remove"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="select_form">
                            <label id="check_all" class="btn btn-primary">{{ trans('labels.select_all') }}</label>
                            <input type="hidden" name="action" value="delete">
                            <button type="submit" style="margin-left: 20px;" class="btn btn-success">{{ trans('labels.delete') }}</button>
                        </div>
                        <div class="form-group"></div>

                        {!! Form::close() !!}
                    @else
                        <div class="alert alert-warning empty" role="alert">
                            Нет фото
                        </div>
                    @endif
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

@section('scripts')
    <script>
        $(function() {
            // Удаление записи
            $('.delete-photo').click( function() {
                var this_check = $(this).closest(".btn-group").find('input[type="checkbox"][name*="check"]');
                $('input[type="checkbox"][name*="check"]').not(this_check).attr('checked', false);
                $(this).closest(".btn-group").find('input[type="checkbox"][name*="check"]').attr('checked', true);
                $(this).closest("form#list").submit();
            });

            // Удаление записей
            $("form#list").submit(function(e) {
                var currentForm = this;
                e.preventDefault();
                if ($("form#list input:checkbox:checked").length > 0){
                    bootbox.confirm("Вы уверены, что хотите удалить выбранные фото?", function (result) {
                        if (result) {
                            currentForm.submit();
                        }
                    });
                }
            });

            // Выделить все
            $("#check_all").on( 'click', function() {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length>0 );
            });
        })
    </script>
@endsection
