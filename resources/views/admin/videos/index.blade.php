@extends('admin.app')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')

                    @if(count($videos) > 0)
                        {!! Form::open(['id'=>'list' ]) !!}
                            <div class="row">
                                @foreach($videos as $video)
                                    <div class="col-sm-6 col-md-4">
                                        <div class="thumbnail">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe class="embed-responsive-item" src="{!! $video->link !!}"  frameborder="0" allowfullscreen></iframe>
                                            </div>
                                            <div class="caption">
                                                <h3>{!! $video->name !!}</h3>
                                                <div class="btn-group photo-btn-group">
                                                    <div class="checkbox checkbox-brand inline pull-left">
                                                        <input style="margin: 0;" name="check[]" value="{{ $video->id }}" id="{!! $video->id !!}" type="checkbox">
                                                        <label for="{!! $video->id !!}"></label>
                                                    </div>
                                                    <a title="Редактировать" href="{!! route('admin.videos.edit', ['video' => $video]) !!}" class="btn btn-info">
                                                        <span class="glyphicon glyphicon-pencil" style="line-height: 22px;"></span>
                                                    </a>
                                                    <button title="Удалить" type="button" class="delete-item btn btn-danger" data-id="{{ $video->id }}">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="select_form">
                                <label id="check_all" class="btn btn-primary">{{ trans('labels.select_all') }}</label>
                                <input type="hidden" name="action" value="delete">
                                <button type="submit" style="margin-left: 20px;" class="btn btn-success">{{ trans('labels.delete') }}</button>
                            </div>
                        {!! Form::close() !!}
                    @else
                        <div class="alert empty warning">
                            {{ trans('labels.videos.empty') }}
                        </div>
                    @endif
                    <a href="{!! route('admin.videos.add') !!}" title="{{ trans('labels.videos.add') }}" class="btn btn-brand btn-flat"><i class="fa fa-plus fa-2x"></i></a>
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

@section('scripts')
    <script>
        $(function() {
            // Удаление записи
            $('.delete-item').click( function() {
                var this_check = $(this).closest(".btn-group").find('input[type="checkbox"][name*="check"]');
                $('input[type="checkbox"][name*="check"]').not(this_check).attr('checked', false);
                $(this).closest(".btn-group").find('input[type="checkbox"][name*="check"]').attr('checked', true);
                $(this).closest("form#list").submit();
            });

            // Удаление записей
            $("form#list").submit(function(e) {
                var currentForm = this;
                e.preventDefault();
                if ($("form#list input:checkbox:checked").length > 0){
                    bootbox.confirm("{{ trans('messages.delete_selected') }}", function (result) {
                        if (result) {
                            currentForm.submit();
                        }
                    });
                }
            });
            // Выделить все
            $("#check_all").on( 'click', function() {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length>0 );
            });
        })
    </script>
@endsection