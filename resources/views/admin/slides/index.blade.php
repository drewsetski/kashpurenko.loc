@extends('admin.app')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')

                    {!! Form::open(array( 'id' => 'upload_form', 'url' => route('admin.slides.store'), 'class' => 'form-horizontal', 'role' => 'form', 'files' => true ) ) !!}

                    <div class="form-group">
                        {!! Form::label('image', trans('labels.images'), array('class'=>'col-sm-2 control-label') ) !!}
                        <div class="col-sm-8">
                            {!! Form::file('image[]', array('class' => 'filestyle', 'data-value'=> '', 'data-buttonText' => trans('labels.slides.chose'), 'data-buttonName' => 'btn-primary', 'data-icon' => 'true' , 'multiple' => 'true') ) !!}
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success">{{ trans('labels.upload') }}</button>
                        </div>
                    </div>

                    {!! Form::close() !!}

                    @if(count($slides) > 0)
                        {!! Form::open(['id'=>'list', 'url' => route('admin.slides.delete'), 'method' => 'DELETE']) !!}
                        <div class="row">
                            @foreach($slides as $slide)
                                <div class="col-sm-6 col-md-3">
                                    <div class="thumbnail">
                                        <img style="max-height: 200px; width: auto; display: block;" alt="icon" src="{{ asset('uploads/slides/' . $slide->image) }}">
                                        <div class="caption">
                                            <div class="btn-group photo-btn-group">
                                                <div class="checkbox checkbox-brand inline pull-left">
                                                    <input style="margin: 0;" name="check[]" value="{{ $slide->id }}" id="{!! $slide->id !!}" type="checkbox">
                                                    <label for="{!! $slide->id !!}"></label>
                                                </div>
                                                <button title="Удалить" type="button" class="delete-item btn btn-danger" data-id="{{ $slide->id }}">
                                                    <span class="glyphicon glyphicon-remove"></span>
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="select_form">
                            <label id="check_all" class="btn btn-primary">{{ trans('labels.select_all') }}</label>
                            <input type="hidden" name="action" value="delete">
                            <button type="submit" style="margin-left: 20px;" class="btn btn-success">{{ trans('labels.delete') }}</button>
                        </div>
                        {!! Form::close() !!}
                    @else
                        <div class="alert empty warning">
                            {{ trans('labels.slides.empty') }}
                        </div>
                    @endif
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

@section('scripts')
    <script>
        $(function() {
            // Удаление записи
            $('.delete-item').click( function() {
                var this_check = $(this).closest(".btn-group").find('input[type="checkbox"][name*="check"]');
                $('input[type="checkbox"][name*="check"]').not(this_check).attr('checked', false);
                $(this).closest(".btn-group").find('input[type="checkbox"][name*="check"]').attr('checked', true);
                $(this).closest("form#list").submit();
            });

            // Удаление записей
            $("form#list").submit(function(e) {
                var currentForm = this;
                e.preventDefault();
                if ($("form#list input:checkbox:checked").length > 0){
                    bootbox.confirm("{{ trans('messages.delete_selected') }}", function (result) {
                        if (result) {
                            currentForm.submit();
                        }
                    });
                }
            });
            // Выделить все
            $("#check_all").on( 'click', function() {
                $('input[type="checkbox"][name*="check"]').prop('checked', $('input[type="checkbox"][name*="check"]:not(:checked)').length>0 );
            });
        })
    </script>
@endsection