@extends('app')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/google-map.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/contact-form.css')}}">
@endsection

@section('header')
    <header class="header__mod" @if($page->main_bg) style="background-image: url({{ url('uploads/pages/' . $page->main_bg) }})" @endif>
        @include('partials._nav')
    </header>
@endsection

@section('content')
    <main>

        <section class="well2" {{ $page->main_color != '' ? "style='background: $page->main_color" : ''}}>
            <div class="container">
                <h3 class="mod-center">{!! $page['name'] !!}</h3>
                <div class="row offset2">
                    <div class="grid_12 contact-list">
                        @foreach( $contacts as $contact)
                            <div class="grid_4">
                                <address class="color-2">{!! $contact['name'] !!}</address>
                                <dl>
                                    <dt>{{ trans('labels.phone') }}:</dt>
                                    <dd>
                                        <a href="callto:{!! $contact['phone'] !!}">{!! $contact['phone'] !!}</a>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>{{ trans('labels.email') }}:</dt>
                                    <dd>
                                        <a href="mailto:{!! $contact['email'] !!}">{!! $contact['email'] !!}</a>
                                    </dd>
                                </dl>
                                <dl>
                                    {!! $contact['description'] !!}
                                </dl>
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </section>


        <section class="well2 bg-content2">
            <div class="container">
                <h3 class="color-2 mod-center">{{ trans('labels.contacts.form') }}</h3>
                <form id="contact-form" class='contact-form' action="{{ route('feedback') }}">
                    {!! csrf_field() !!}
                    <div class="contact-form-loader"></div>
                    <fieldset class="row">
                        <label class="name grid_4 wow fadeIn fadeInLeft">
                            <input type="text" name="name" placeholder="{{ trans('labels.firstname') }}:" value="" data-constraints="@Required"/>

                            <span class="empty-message">*{{ trans('validation.filled',['attribute' => trans('labels.name')]) }}.</span>
                        </label>

                        <label class="phone grid_4">
                            <input type="text" name="phone" placeholder="{{ trans('labels.phone') }}:" value="" data-constraints="@JustNumbers"/>

                            <span class="empty-message">*{{ trans('validation.filled',['attribute' => trans('labels.phone')]) }}.</span>
                            <span class="error-message">*{{ trans('validation.url',['attribute' => trans('labels.phone')]) }}.</span>
                        </label>

                        <label class="email grid_4  wow fadeIn fadeInRight">
                            <input type="text" name="email" placeholder="{{ trans('labels.email') }}:" value="" data-constraints="@Required @Email"/>

                            <span class="empty-message">*{{ trans('validation.filled',['attribute' => trans('labels.email')]) }}.</span>
                            <span class="error-message">*{{ trans('validation.url',['attribute' => trans('labels.email')]) }}.</span>
                        </label>

                        <label class="message grid_12  wow fadeIn fadeInUp">
                            <textarea name="message" placeholder="{{ trans('labels.message') }}:" data-constraints='@Required'></textarea>

                            <span class="empty-message">*{{ trans('validation.filled',['attribute' => trans('labels.message')]) }}.</span>
                        </label>

                    </fieldset>

                    <div class="btn-wr">
                        <a class="btn4 pulse pulse__mod" href="#" data-type="submit">{{ trans('labels.send') }}</a>
                    </div>

                    <div class="modal fade response-message">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">
                                        &times;
                                    </button>
                                    <h4 class="modal-title">{{ trans('messages.thanks_message') }}</h4>
                                </div>
                                <div class="modal-body">
                                    {{ trans('messages.sent') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>


    </main>
@endsection