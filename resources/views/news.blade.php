@extends('app')

@section('header')
    <header class="header__mod" @if($page->main_bg) style="background-image: url({{ url('uploads/pages/' . $page->main_bg) }})" @endif>
        @include('partials._nav')
    </header>
@endsection

@section('content')
    <main>

        <section class="well2 well2__ins" {{ $page->main_color != '' ? "style='background: $page->main_color" : ''}}>
            <div class="container">
                <h3 class="mod-center">{!! $page['name'] !!}</h3>
                @foreach($news->chunk(2) as $two_articles)
                    <div class="row offset3">
                        @foreach($two_articles as $key => $article)
                            <div class="grid_6">
                                <article>
                                    <img class="wow {!! $wowClasses[$key] !!}" data-wow-duration="2s" src="{!! asset('uploads/news/' . $article['image']) !!}" alt="{!! $article['name'] !!}"/>
                                    <time datetime="{!! $article->date !!}">{!! $article['human date'] !!}</time>
                                    <h4 class="color-2"><a class="a__mod" href="{!! route('article', ['article' => $article->slug]) !!}">{!! $article['name'] !!}</a></h4>

                                    <p class="p__mod1">{!! $article['description'] !!}</p>
                                </article>
                            </div>
                        @endforeach
                    </div>
                @endforeach
                {{ $news->links() }}
            </div>
        </section>


        @if($page['additional_body'] != '')
            <section class="well2 well2__ins parallax center" data-url="images/parallax3.jpg" data-mobile="true" data-speed="0.6">
                <div class="container wow fadeInUp" data-wow-duration="2s">
                    @if(isset( $page['title_additional_body'] ))
                        <h3 class="color-1">
                            {!! $page['title_additional_body'] !!}
                        </h3>
                    @endif
                    @if(isset( $page['slogan_additional_body'] ))
                        <h4 class="color-2 h4__mod">
                            {!! $page['slogan_additional_body'] !!}
                        </h4>
                    @endif

                    <p class="color-1 p-mod1">
                        {!! $page['additional_body'] !!}
                    </p>
                </div>
            </section>
        @endif


    </main>
@endsection