@extends('app')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/camera.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/subscribe-form.css')}}">
@endsection

@section('header')
    <header class="header__mod" @if($main_bg) style="background-image: url({{ url('uploads/pages/' . $main_bg) }})" @endif>
        @include('partials._nav')
    </header>
@endsection

@section('content')
    <main>

        <section class="well well__ins" {{ $main_color != '' ? "style='background: $main_color" : ''}}>
            <div class="container">
                <h3 class="mod-center">{{ trans('labels.music.musics') }}</h3>
                <div class="row">
                    <div class="fixed-player">
                        <audio id="player" preload="auto"></audio>
                    </div>
                    <div class="block2 grid_12 mg-add2">
                        <ul class="music-list" data-player-id="player">
                            @for($i = 0; $i < count($musics); $i++)
                                <li @if($i % 2 == 0) class='li__skin' @endif data-src="{{ url('uploads/music/' . $musics[$i]->file) }}"><h4 class="h4__mod">{{ $musics[$i]->name }}<br>
                                        <span class="color-2">{{ $musics[$i]->category->name }}</span></h4></li>
                            @endfor
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection