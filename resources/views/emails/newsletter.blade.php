<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
    <h2>{!! trans('emails.article_added_header_text', ['article' => $article->name]) !!}</h2>
    @if ($article->image)
        <img src="{{ url('uploads/news/' . $article->image) }}" alt="{{ $article->name }}">
    @endif
    <p>{!! trans('emails.article_added_body', ['link' => url('news/' . $article->id)]) !!}</p>
</body>
</html>