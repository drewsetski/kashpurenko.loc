<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
    <h2>{{ trans('labels.message_from', ['name' => $unserialized['name']]) }}</h2>
    <br/> <br/>
    <p><strong>{{ trans('labels.phone_number') }}:</strong> {{ $unserialized['phone'] }}</p>
    <p><strong>{{ trans('labels.message') }}:</strong> {{ $unserialized['message'] }}</p>
</body>
</html>