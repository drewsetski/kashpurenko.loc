@extends('app')

@section('header')
    <header class="header__mod" @if($main_bg) style="background-image: url({{ url('uploads/pages/' . $main_bg) }})" @endif>
        @include('partials._nav')
    </header>
@endsection

@section('content')
    <main>

        <section class="well2 well2__ins">
            <div class="container">
                <div class="row">
                    <div class="grid_12">
                        <h2 class="mg-add center">
                            <span>
                                {!! $article['name'] !!}
                            </span>
                        </h2>
                        <h4 class="color-2 center">
                            <img class="wow slideInRight" data-wow-duration="2s" src="{!! asset('uploads/news/' . $article['image']) !!}" alt="{!! $article['name'] !!}"/>
                        </h4>
                        {!! $article['body'] !!}
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection