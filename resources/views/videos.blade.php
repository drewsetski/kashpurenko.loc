@extends('app')

@section('header')
    <header class="header__mod" @if($page->main_bg) style="background-image: url({{ url('uploads/pages/' . $page->main_bg) }})" @endif>
        @include('partials._nav')
    </header>
@endsection

@section('content')
    <main>

        <section class="well2 well2__ins" {{ $page->main_color != '' ? "style='background: $page->main_color" : ''}}>
            <div class="container">
                <h3 class="mod-center">{!! $page['name'] !!}</h3>
                @foreach($videos->chunk(2) as $two_videos)
                    <div class="row offset3">
                        @foreach($two_videos as $key => $video)
                            <div class="grid_6">
                                <article>
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="{!! $video['link'] !!}"  frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <h4 class="color-2">{!! $video['name'] !!}</h4>
                                </article>
                            </div>
                        @endforeach
                    </div>
                @endforeach
                {{ $videos->links() }}
            </div>
        </section>


        @if($page['additional_body'] != '')
            <section class="well2 well2__ins parallax center" data-url="@if($page->additional_bg) {{ url('uploads/pages/' . $page->additional_bg) }} @else {{ url('images/parallax2.jpg') }} @endif" data-mobile="true" data-speed="0.6">
                <div class="container wow fadeInUp" data-wow-duration="2s">
                    @if(isset( $page['title_additional_body'] ))
                        <h3 class="color-1">
                            {!! $page['title_additional_body'] !!}
                        </h3>
                    @endif
                    @if(isset( $page['slogan_additional_body'] ))
                        <h4 class="color-2 h4__mod">
                            {!! $page['slogan_additional_body'] !!}
                        </h4>
                    @endif

                    <p class="color-1 p-mod1">
                        {!! $page['additional_body'] !!}
                    </p>
                </div>
            </section>
        @endif


    </main>
@endsection