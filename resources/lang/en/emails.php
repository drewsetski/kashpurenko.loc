<?php
/**
 * Created by PhpStorm.
 * User: shox
 * Date: 28.04.2016
 * Time: 10:20
 */

return [
    'email' => 'admin@' . env('APP_DOMAIN'),
    'sender' => 'Kashpurenko Vladislav',
    'article_added_header_text' => 'Владислав Кашпуренко опубликовал новость <b>:article</b>>',
    'article_added_body' => 'Перейдите по ссылке, чтобы узнать подробнее <b>:link</b>>'
];
